##Description:##
**The ScheduleUtils library is an open source library for the Android platform. This library facilitates working with schedule/appointment items and hours of business.**

**Supports Android API 16+**

**Features:**

1. Add and edit schedule items while checking that the items do not constitute double bookings, or simply delete them.

1. Sort schedule items in ascending or descending order based upon start time, duration or end time.

1. Find out when next availability slot is based upon hours of business, schedule items and a given time. (Method has now passed all tests)

1. Retrieve a list of available slots between times given a list of schedule items and hours of business. (Method has now passed all tests)

1. Check if time is within business hours on particular day of the week.

1. Check if there is a schedule item in progress at a given time.

1. Check if an entity is available at a given time bases upon a list of schedule items and hours of business.

1. Get opening hours for a given day.

1. Find when a business is next open on or after a given date time.

1. Use library to see if there are appointments for a certain day.

**Library is distributed with demo app (APK API 16+). You can also find this app on the Google Play Store here: https://play.google.com/store/apps/details?id=com.WarwickWestonWright.ScheduleUtilsDemo**

***You can make a donation the HG Widgets for Android here: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=DWT2TT7X83PE8***

You can contact the developer at warwickwestonwright@live.com

LICENSE. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License

In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".

Copyright (c) 2018, Warwick Weston Wright All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS

FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,

BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,

STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.