/*
LICENSE. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
Copyright (c) 2018, Warwick Weston Wright All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
/*
Class Description: A Utility Class for sorting schedule items and making useful calculations based on hours of business and schedule items.
Class Usage:
Simply construct and call the methods. See method descriptions comments above each method for more details.

IMPORTANT NOTE: Some methods will NOT work properly when scheduleItems are not ordered ascending by start date
*/
package com.WarwickWestonWright.ScheduleUtils.UtilityFunctions;

import com.WarwickWestonWright.ScheduleUtils.LibConstants;
import com.WarwickWestonWright.ScheduleUtils.ScheduleItems.ScheduleItem;
import com.WarwickWestonWright.ScheduleUtils.ScheduleItems.SortableItem;
import com.WarwickWestonWright.ScheduleUtils.WorkingHours.FromToByDay;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import static com.WarwickWestonWright.ScheduleUtils.LibConstants.MILLISECONDS_PER_HOUR;
import static com.WarwickWestonWright.ScheduleUtils.LibConstants.MILLISECONDS_PER_MINUTE;
import static com.WarwickWestonWright.ScheduleUtils.LibConstants.MILLISECONDS_PER_SECOND;

/* IMPORTANT NOTE: Some methods will NOT work properly when scheduleItems are not ordered ascending by start date */
public class UtilityFunctions {

	public static final int START = 1;
	public static final int DURATION = 2;
	public static final int END = 3;
	private List<ScheduleItem> scheduleItems = new ArrayList<>();
	private List<FromToByDay> fromToByDays = new ArrayList<>();
	private boolean isSortedFlag;

	private GregorianCalendar calendar = new GregorianCalendar();

	public UtilityFunctions() {}

	/* Sorts List<ScheduleItem> ascending or descending (second parameter).
	Can be used to sort by start time, duration or end time (first parameter). Third parameter being the subject.
	Used internally public for Convenience. */
	public List<ScheduleItem> getSortedScheduleItems(final int byWhat, final boolean asc, final List<ScheduleItem> scheduleItems) {

		final List<SortableItem> sortableItems = new ArrayList<>();
		final int scheduleItemsSize = scheduleItems.size();

		for(int i = 0; i < scheduleItemsSize; i++) {
			final SortableItem sortableItem = new SortableItem();
			switch(byWhat) {
				case START:
					sortableItem.setSortableTag(scheduleItems.get(i).getStartTime().getTime());
					break;
				case DURATION:
					sortableItem.setSortableTag(scheduleItems.get(i).getDuration());
					break;
				case END:
					sortableItem.setSortableTag(scheduleItems.get(i).getEndTime().getTime());
					break;
				default: break;
			}//End switch(byWhat)

			sortableItem.setStartTime(scheduleItems.get(i).getStartTime());
			sortableItem.setDuration(scheduleItems.get(i).getDuration());
			sortableItem.setEndTime(scheduleItems.get(i).getEndTime());
			sortableItems.add(sortableItem);

		}//End for(int i = 0; i < scheduleItemsSize; i++)

		if(asc == true) {
			return scheduleItemsAsc(sortableItems);
		}
		else /* if(asc == false) */ {
			return scheduleItemsDesc(sortableItems);
		}

	}//End public List<ScheduleItem> getSortedScheduleItems(final int byWhat, final boolean asc, final List<ScheduleItem> scheduleItems)


	/* Parsing fromToByDays; returns a value indicating if open for business at the time
	indicated by atTime.
	Algorithm is: if(atTime >= from and atTime < to) return true otherwise false
	Used internally public for Convenience. */
	public boolean isOpen(final Date atTime, final List<FromToByDay> fromToByDays) {
		if(fromToByDays == null || fromToByDays.size() != 7) {
			return true;
		}

		calendar.setFirstDayOfWeek(GregorianCalendar.SUNDAY);
		calendar.setTime(atTime);
		final int dow = calendar.get(Calendar.DAY_OF_WEEK);
		final FromToByDay fromToByDay = new FromToByDay();

		for(FromToByDay ttbd : fromToByDays) {
			if(ttbd.getDay() == dow) {
				fromToByDay.setDay(ttbd.getDay());
				fromToByDay.setFrom(ttbd.getFrom());
				fromToByDay.setTo(ttbd.getTo());
				break;
			}
		}

		if(fromToByDay.getFrom().equals("") || fromToByDay.getTo().equals("")) {
			return false;
		}

		final String[] from = fromToByDay.getFrom().split(":");
		final String[] to = fromToByDay.getTo().split(":");
		long fromMilli;
		long toMilli;

		//Calculate from in milliseconds
		switch(from.length) {
			case 2: fromMilli = (Integer.parseInt(from[0]) * MILLISECONDS_PER_HOUR) +
				(Integer.parseInt(from[1]) * MILLISECONDS_PER_MINUTE);
				break;
			case 3: fromMilli = (Integer.parseInt(from[0]) * MILLISECONDS_PER_HOUR) +
				(Integer.parseInt(from[1]) * MILLISECONDS_PER_MINUTE) +
				(Integer.parseInt(from[2]) * MILLISECONDS_PER_SECOND);
				break;
			case 4: fromMilli = (Integer.parseInt(from[0]) * MILLISECONDS_PER_HOUR) +
				(Integer.parseInt(from[1]) * MILLISECONDS_PER_MINUTE) +
				(Integer.parseInt(from[2]) * MILLISECONDS_PER_SECOND) +
				(Integer.parseInt(from[3]));
				break;
			case 1: fromMilli = Integer.parseInt(from[0]) * MILLISECONDS_PER_HOUR;
				break;
			case 0: fromMilli = 0;
				break;
			default: fromMilli = (Integer.parseInt(from[0]) * MILLISECONDS_PER_HOUR) +
				(Integer.parseInt(from[1]) * MILLISECONDS_PER_MINUTE) +
				(Integer.parseInt(from[2]) * MILLISECONDS_PER_SECOND);
				//5th element+ ignored
				break;
		}//End switch(from.length)

		//Calculate to in milliseconds
		switch(to.length) {
			case 2: toMilli = (Integer.parseInt(to[0]) * MILLISECONDS_PER_HOUR) +
				(Integer.parseInt(to[1]) * MILLISECONDS_PER_MINUTE);
				break;
			case 3: toMilli = (Integer.parseInt(to[0]) * MILLISECONDS_PER_HOUR) +
				(Integer.parseInt(to[1]) * MILLISECONDS_PER_MINUTE) +
				(Integer.parseInt(to[2]) * MILLISECONDS_PER_SECOND);
				break;
			case 4: toMilli = (Integer.parseInt(to[0]) * MILLISECONDS_PER_HOUR) +
				(Integer.parseInt(to[1]) * MILLISECONDS_PER_MINUTE) +
				(Integer.parseInt(to[2]) * MILLISECONDS_PER_SECOND) +
				(Integer.parseInt(to[3]));
				break;
			case 1: toMilli = Integer.parseInt(to[0]) * MILLISECONDS_PER_HOUR;
				break;
			case 0: toMilli = 0;
				break;
			default: toMilli = (Integer.parseInt(to[0]) * MILLISECONDS_PER_HOUR) +
				(Integer.parseInt(to[1]) * MILLISECONDS_PER_MINUTE) +
				(Integer.parseInt(to[2]) * MILLISECONDS_PER_SECOND);
				//5th element+ ignored
				break;
		}//End switch(to.length)

		//Calculate atTime in milliseconds
		final long atMilli = (calendar.get(GregorianCalendar.HOUR_OF_DAY) * MILLISECONDS_PER_HOUR) +
			(calendar.get(GregorianCalendar.MINUTE) * MILLISECONDS_PER_MINUTE) +
			(calendar.get(GregorianCalendar.SECOND) * MILLISECONDS_PER_SECOND);

		if(atMilli >= fromMilli && atMilli < toMilli) {
			return true;
		}
		else /* if(atMilli < fromMilli || atMilli >= toMilli) */ {
			return false;
		}

	}//End public boolean isOpen(final Date atTime, final List<FromToByDay> fromToByDays)


	/* Detects if availability slot exists at time (independent of working hours) if atTime conflicts
	with a schedule item index; returns that index otherwise returns -1.
	Used internally public for Convenience. */
	public int hasAppointmentAtTime(final Date atTime, final List<ScheduleItem> scheduleItems) {

		if(scheduleItems == null || scheduleItems.size() == 0) {
			return -1;
		}

		if(isSortedFlag == false) {
			this.scheduleItems.clear();
			this.scheduleItems.addAll(scheduleItems);
			this.scheduleItems = getSortedScheduleItems(START, true, scheduleItems);
			isSortedFlag = true;
		}

		final int scheduleItemsSize = scheduleItems.size();
		for(int i = 0; i < scheduleItemsSize; i++) {
			if(atTime.getTime() >= this.scheduleItems.get(i).getStartTime().getTime() && atTime.getTime() < this.scheduleItems.get(i).getEndTime().getTime()) {
				return i;
			}
		}

		return -1;
	}//End public int hasAppointmentAtTime(final Date atTime, final List<ScheduleItem> scheduleItems)


	/* Detects if currently available based on atTime, fromToByDays(Hours of Business) and scheduleItems.
	Used internally public for Convenience. */
	public boolean isAvailableAt(final Date atTime, final List<ScheduleItem> scheduleItems, final List<FromToByDay> fromToByDays) {

		if(isOpen(atTime, fromToByDays) == true) {
			if(hasAppointmentAtTime(atTime, scheduleItems) == -1) {
				return true;
			}
			else /* if(hasAppointmentAtTime(atTime, scheduleItems) > -1) */ {
				return false;
			}
		}
		else /* if(isOpen(atTime, fromToByDays) == false) */ {
			return false;
		}//End if(isOpen(atTime, fromToByDays) == true)

	}//End public boolean isAvailableAt(final Date atTime, final List<ScheduleItem> scheduleItems, final List<FromToByDay> fromToByDays)


	/* Returns the beginning for the day 00:00 plus dayOffset. Used internally public for Convenience. */
	public Date getDayStart(final Date atTime, final int dayOffset) {
		calendar = new GregorianCalendar();
		calendar.setTime(atTime);
		calendar = new GregorianCalendar(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE) + dayOffset);
		return calendar.getTime();
	}//End public Date getDayStart(final Date atTime, final int dayOffset)


	/* Returns opening time (first element) and closing time (second element).
	This is based on the date of the atTime and hours of business (fromToByDays second parameter)
	i.e. if atTime falls on a Wednesday returns the hours of business for Wednesday.
	If closed for business that day returns null
	Used internally public for Convenience. */
	public Date[] getOpeningHours(final Date atTime, final List<FromToByDay> fromToByDays) {

		calendar.setFirstDayOfWeek(GregorianCalendar.SUNDAY);
		calendar.setTime(atTime);
		final int dow = calendar.get(Calendar.DAY_OF_WEEK);
		final FromToByDay fromToByDay = new FromToByDay();

		for(FromToByDay ttbd : this.fromToByDays) {
			if(ttbd.getDay() == dow) {
				fromToByDay.setDay(ttbd.getDay());
				fromToByDay.setFrom(ttbd.getFrom());
				fromToByDay.setTo(ttbd.getTo());
				break;
			}
		}

		if(fromToByDay.getFrom().equals("") || fromToByDay.getTo().equals("")) {
			return null;
		}

		final Date[] returnVal = new Date[2];
		final String[] from = fromToByDay.getFrom().split(":");
		final String[] to = fromToByDay.getTo().split(":");
		int hourOfDay = 0; int minute = 0; int second = 0; int milli = 0;

		//Calculate opening time
		switch(from.length) {
			case 2: hourOfDay = Integer.parseInt(from[0]);
				minute = Integer.parseInt(from[1]);
				second = 0;
				break;
			case 3: hourOfDay = Integer.parseInt(from[0]);
				minute = Integer.parseInt(from[1]);
				second = Integer.parseInt(from[2]);
				break;
			case 4: hourOfDay = Integer.parseInt(from[0]);
				minute = Integer.parseInt(from[1]);
				second = Integer.parseInt(from[2]);
				milli = Integer.parseInt(from[3]);
				break;
			case 1: hourOfDay = Integer.parseInt(from[0]);
				minute = 0;
				second = 0;
				break;
			default: //ToDo throw error;
				break;
		}//End switch(from.length)

		returnVal[0] = new Date((new GregorianCalendar(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE),
			hourOfDay, minute, second)).getTime().getTime() + milli);

		//Calculate closing time
		switch(to.length) {
			case 2: hourOfDay = Integer.parseInt(to[0]);
				minute = Integer.parseInt(to[1]);
				second = 0;
				break;
			case 3: hourOfDay = Integer.parseInt(to[0]);
				minute = Integer.parseInt(to[1]);
				second = Integer.parseInt(to[2]);
				break;
			case 4: hourOfDay = Integer.parseInt(to[0]);
				minute = Integer.parseInt(to[1]);
				second = Integer.parseInt(to[2]);
				milli = Integer.parseInt(to[3]);
				break;
			case 1: hourOfDay = Integer.parseInt(to[0]);
				minute = 0;
				second = 0;
				break;
			default: //ToDo throw error;
				break;
		}//End switch(to.length)

		returnVal[1] = new Date((new GregorianCalendar(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE),
			hourOfDay, minute, second)).getTime().getTime() + milli);
		return returnVal;

	}//End public Date[] getOpeningHours(final Date atTime, final List<FromToByDay> fromToByDays)


	/* Returns next opening time starting from atTime and returns the opening time for that day,
	unless closed for that day in which case finds the next available opening time.
	Used internally public for Convenience. */
	public Date getNextOpeningTime(final Date atTime, final List<FromToByDay> fromToByDays) {

		this.fromToByDays = set247(fromToByDays);

		calendar.setFirstDayOfWeek(GregorianCalendar.SUNDAY);
		calendar.setTime(atTime);
		final int[] days = new int[7];
		final int startDay = calendar.get(Calendar.DAY_OF_WEEK);

		for(int i = startDay; i <  startDay + 7; i++) {
			days[i - startDay] = ((i % 7) + 1) - 1;
			if(days[i - startDay] == 0) {days[i - startDay] = 7;}
		}

		int hourOfDay = 0; int minute = 0; int second = 0; int milli = 0;
		FromToByDay fromToByDay = null;

		for(int i = 0; i < 7; i++) {
			int fromToByDaysIterator = 0;
			while(true) {

				if(this.fromToByDays.get(fromToByDaysIterator).getDay() == days[i]) {
					fromToByDay = this.fromToByDays.get(fromToByDaysIterator);
					break;
				}

				fromToByDaysIterator++;
				if(fromToByDaysIterator > 6) {break;}
			}//End while(true)

			if(fromToByDay.getFrom().equals("")) {continue;}

			final String[] from = fromToByDay.getFrom().split(":");

			switch(from.length) {
				case 2: hourOfDay = Integer.parseInt(from[0]);
					minute = Integer.parseInt(from[1]);
					second = 0;
					break;
				case 3: hourOfDay = Integer.parseInt(from[0]);
					minute = Integer.parseInt(from[1]);
					second = Integer.parseInt(from[2]);
					break;
				case 4: hourOfDay = Integer.parseInt(from[0]);
					minute = Integer.parseInt(from[1]);
					second = Integer.parseInt(from[2]);
					milli = Integer.parseInt(from[3]);
					break;
				case 1: hourOfDay = Integer.parseInt(from[0]);
					minute = 0;
					second = 0;
					break;
				default: //ToDo throw error;
					break;
			}//End switch(from.length)

			return new Date((new GregorianCalendar(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE) + i,
				hourOfDay, minute, second).getTime()).getTime() + milli);

		}//End for(int i = 0; i < 7; i++)

		return null;

	}//End public Date getNextOpeningTime(final Date atTime, final List<FromToByDay> fromToByDays)


	/* Returns a value indicating if there is a schedule item for the same day as atTime.
	Value returned is the index of the first schedule item for that day or -1 if no schedule item exists for that day.
	Used internally public for Convenience.*/
	public int hasItemForToday(final Date atTime, final List<ScheduleItem> scheduleItems) {

		if(scheduleItems == null || scheduleItems.size() == 0) {
			return -1;
		}

		if(isSortedFlag == false) {
			this.scheduleItems.clear();
			this.scheduleItems.addAll(scheduleItems);
			this.scheduleItems = getSortedScheduleItems(START, true, scheduleItems);
			isSortedFlag = true;
		}

		Date startOfDay = getDayStart(atTime, 0);
		final int scheduleItemsSize = scheduleItems.size();
		for(int i = 0; i < scheduleItemsSize; i++) {
			Date tempDate = getDayStart(this.scheduleItems.get(i).getStartTime(), 0);
			if(startOfDay.getTime() == tempDate.getTime()) {
				return i;
			}
		}

		return -1;
	}//End public int hasItemForToday(final Date atTime, final List<ScheduleItem> scheduleItems)


	/* Gets the index of the first scheduleItems (startTime or endTime) after given time depending on startTime parameter
	returns the index of the found item or -1 if not found */
	public int getFirstItemAfter(final Date afterTime, final List<ScheduleItem> scheduleItems, final boolean startTime) {
 		int scheduleItemsSize = scheduleItems.size();
 		if(scheduleItems == null || scheduleItemsSize == 0) {return -1;}

 		for(int i = 0; i < scheduleItemsSize; i++) {
 			if(startTime == true) {
 				if(scheduleItems.get(i).getStartTime().getTime() > afterTime.getTime()) {
 					return i;
				}
			}
			else if(startTime == false) {
				if(scheduleItems.get(i).getEndTime().getTime() > afterTime.getTime()) {
					return i;
				}
			}
		}
		return -1;
	}//End public int getFirstItemAfter(final Date afterTime, final List<ScheduleItem> scheduleItems, final boolean startTime)


	/* Gets the day difference between subject and plusDate based upon the beginning of each day (00:00:00)
	if plusDate falls 1 day after subject then returns 1, if it falls 2 days before -2 or 0 for the same day */
	public int getDayDifference(final Date subject, final Date plusDate) {
		final Date subjectStart = getDayStart(subject, 0);
		final Date plusDateStart = getDayStart(plusDate, 0);
		return (int) ((subjectStart.getTime() - plusDateStart.getTime()) / LibConstants.MILLISECONDS_PER_DAY);
	}


	/* Retrieves a list of schedule items for given day. Item is included if any of start time or end time falls on given day */
	public List<ScheduleItem> getItemsForDay(final Date day, final List<ScheduleItem> scheduleItems) {
		final List<ScheduleItem> returnVal = new ArrayList<>();
		for(ScheduleItem item : scheduleItems) {
			if(getDayDifference(item.getStartTime(), day) == 0) {
				returnVal.add(item);
			}
			else if(getDayDifference(item.getEndTime(), day) == 0) {
				returnVal.add(item);
			}
		}//End for(ScheduleItem item : scheduleItems)
		return returnVal;
	}//End public List<ScheduleItem> getItemsForDay(final Date day, final List<ScheduleItem> scheduleItems)


	//Returns the number of items that exist after a given index (positive if more exist, negative if idx is past the end index or 0 if idx is at the last index)
	public int moreItems(final int idx, final List<ScheduleItem> scheduleItems) {

		final int scheduleItemsSize = scheduleItems.size();
		if(idx > scheduleItemsSize - 1) {
			return -1;
		}
		else if(idx <= scheduleItemsSize - 1) {
			return scheduleItemsSize - 1 - idx;
		}

		return scheduleItems.size() - idx + 1;
	}//End public int moreItems(final int idx, final List<ScheduleItem> scheduleItems)


	/* Used internally public for Convenience. Sorts List<ScheduleItem> ascending */
	public List<ScheduleItem> scheduleItemsAsc(final List<SortableItem> sortableItems) {

		final List<ScheduleItem> returnVal = new ArrayList<>();
		final int sortableItemsSize = sortableItems.size();

		if(sortableItemsSize < 1) {
			return returnVal;
		}
		boolean swap;
		int n = sortableItems.size() - 1;

		do {
			swap = false;
			for(int i = 0; i < n; i++) {
				//> for ascending < for descending

				if(sortableItems.get(i).getSortableTag() > sortableItems.get(i + 1).getSortableTag()) {
					Collections.swap(sortableItems, i, i + 1);
					swap = true;
				}
			}
			n--;
		} while(swap);

		for(int i = 0; i < sortableItemsSize; i++) {
			final ScheduleItem scheduleItem = new ScheduleItem(sortableItems.get(i).getStartTime(), sortableItems.get(i).getDuration());
			returnVal.add(scheduleItem);
		}

		return returnVal;

	}//End public List<ScheduleItem> scheduleItemsAsc(final List<SortableItem> sortableItems)


	/* Used internally public for Convenience. Sorts List<ScheduleItem> descending */
	public List<ScheduleItem> scheduleItemsDesc(final List<SortableItem> sortableItems) {

		final List<ScheduleItem> returnVal = new ArrayList<>();
		final int sortableItemsSize = sortableItems.size();

		if(sortableItemsSize < 1) {
			return returnVal;
		}
		boolean swap;
		int n = sortableItems.size() - 1;

		do {
			swap = false;
			for(int i = 0; i < n; i++) {
				//> for ascending < for descending

				if(sortableItems.get(i).getSortableTag() < sortableItems.get(i + 1).getSortableTag()) {
					Collections.swap(sortableItems, i, i + 1);
					swap = true;
				}
			}
			n--;
		} while(swap);

		for(int i = 0; i < sortableItemsSize; i++) {
			final ScheduleItem scheduleItem = new ScheduleItem(sortableItems.get(i).getStartTime(), sortableItems.get(i).getDuration());
			returnVal.add(scheduleItem);
		}

		return returnVal;

	}//End public List<ScheduleItem> scheduleItemsDesc(final List<SortableItem> sortableItems)


	/* Gets the day of the week 1 - 7 Sunday to Saturday respectively */
	public int getDOW(Date date) {
		if(date != null) {
			calendar.setTime(date);
		}
		else if(date == null) {
			calendar.setTime(new Date());
		}
		return calendar.get(Calendar.DAY_OF_WEEK);
	}//End public int getDOW(Date date)


	public List<FromToByDay> set247(final List<FromToByDay> fromToByDays) {

		this.fromToByDays = new ArrayList<>();
		if(fromToByDays == null || fromToByDays.size() != 7) {
			for(int i = 1; i < 8; i++) {
				this.fromToByDays.add(new FromToByDay("00", "23:59:59:999", i));
			}
		}
		else if(fromToByDays != null && fromToByDays.size() == 7) {
			this.fromToByDays.addAll(fromToByDays);
		}

		return this.fromToByDays;

	}//End public List<FromToByDay> set247(final List<FromToByDay> fromToByDays)

	public boolean checkIs247(final List<FromToByDay> fromToByDays) {
		for(FromToByDay fromToByDay: fromToByDays) {
			if(fromToByDay.getFrom().equals("00") == false || fromToByDay.getTo().equals("23:59:59:999") == false) {
				return false;
			}
		}
		return true;
	}//End public boolean checkIs247(final List<FromToByDay> fromToByDays)

}