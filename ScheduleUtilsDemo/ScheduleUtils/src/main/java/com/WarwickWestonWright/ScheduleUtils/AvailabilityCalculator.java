/*
LICENSE. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
Copyright (c) 2018, Warwick Weston Wright All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
/*
Class Usage:
Construct the object and call the method Date getNextAvailable(final Date fromTime, final List<ScheduleItem> scheduleItems, final List<FromToByDay> fromToByDays).
This method calculates when a next appointment slot is available. See method description below
Note: The non-default constructor only acts as a holder for the scheduleItems and fromToByDays objects as are the getters and setters

IMPORTANT NOTE: Some methods will NOT work properly when scheduleItems are not ordered ascending by start date
*/
package com.WarwickWestonWright.ScheduleUtils;

import com.WarwickWestonWright.ScheduleUtils.ScheduleItems.ScheduleItem;
import com.WarwickWestonWright.ScheduleUtils.UtilityFunctions.UtilityFunctions;
import com.WarwickWestonWright.ScheduleUtils.WorkingHours.FromToByDay;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.WarwickWestonWright.ScheduleUtils.LibConstants.MILLISECONDS_PER_DAY;
import static com.WarwickWestonWright.ScheduleUtils.UtilityFunctions.UtilityFunctions.START;

/* IMPORTANT NOTE: Some methods will NOT work properly when scheduleItems are not ordered ascending by start date */

public class AvailabilityCalculator {

	private List<ScheduleItem> scheduleItems = new ArrayList<>();
	private List<FromToByDay> fromToByDays;
	private Date gapsEndAnchor;
	private boolean isSortedFlag;
	private final UtilityFunctions utilityFunctions = new UtilityFunctions();

	public AvailabilityCalculator() {}

	public AvailabilityCalculator(final List<ScheduleItem> scheduleItems, final List<FromToByDay> fromToByDays) {
		if(isSortedFlag == false) {
			this.scheduleItems.clear();
			this.scheduleItems.addAll(scheduleItems);
			this.scheduleItems = utilityFunctions.getSortedScheduleItems(START, true, scheduleItems);
			isSortedFlag = true;
		}
		this.fromToByDays = fromToByDays;
	}

	//Setters
	public void setScheduleItems(final List<ScheduleItem> scheduleItems) {
		if(isSortedFlag == false) {
			this.scheduleItems.clear();
			this.scheduleItems.addAll(scheduleItems);
			this.scheduleItems = utilityFunctions.getSortedScheduleItems(START, true, scheduleItems);
			isSortedFlag = true;
		}
	}

	public void setFromToByDays(final List<FromToByDay> fromToByDays) {this.fromToByDays = fromToByDays;}

	//Getters
	public List<ScheduleItem> getScheduleItems() {return this.scheduleItems;}
	public List<FromToByDay> getFromToByDays() {return this.fromToByDays;}
	
	/* Calculates the next available slot based upon appointments (second parameter) and working hours (third parameter).
	Returns the Date of the first available slot at or after fromTime (First parameter).
	Notes: Parsing null for working hours will assume hours of business as 24/7.
	When fromTime coincides with a currently available slot (during hours of business),
	return value will be equal to the fromTime parameter. */
	public Date getNextAvailable(final Date fromTime, final List<ScheduleItem> scheduleItems, final List<FromToByDay> fromToByDays) {

		this.fromToByDays = utilityFunctions.set247(fromToByDays);

		if(utilityFunctions.isAvailableAt(fromTime, this.scheduleItems, this.fromToByDays) == true) {return fromTime;}
		
		Date tomorrow;
		Date[] hours = utilityFunctions.getOpeningHours(fromTime, this.fromToByDays);

		if(isSortedFlag == false) {
			this.scheduleItems.clear();
			this.scheduleItems.addAll(scheduleItems);
			this.scheduleItems = utilityFunctions.getSortedScheduleItems(START, true, scheduleItems);
			isSortedFlag = true;
		}

		if(utilityFunctions.isAvailableAt(fromTime, this.scheduleItems, this.fromToByDays) == true) {
			return fromTime;
		}

		int scheduleItemsSize;
		if(this.scheduleItems == null || this.scheduleItems.size() == 0) {
			scheduleItemsSize = 0;
			if(hours == null) {
				tomorrow = utilityFunctions.getDayStart(fromTime, 1);
				return utilityFunctions.getNextOpeningTime(tomorrow, this.fromToByDays);
			}
			else if(hours != null) {
				if(hours[0].getTime() >= fromTime.getTime()) {
					return hours[0];
				}
				else if(hours[0].getTime() < fromTime.getTime()) {
					if(fromTime.getTime() >= hours[1].getTime()) {
						tomorrow = utilityFunctions.getDayStart(fromTime, 1);
						return utilityFunctions.getNextOpeningTime(tomorrow, this.fromToByDays);
					}
					else if(fromTime.getTime() < hours[1].getTime()) {
						return fromTime;
					}
				}

			}//End if(hours == null)
		}
		else {
			scheduleItemsSize = this.scheduleItems.size();
		}//End if(this.scheduleItems == null || this.scheduleItems.size() == 0)

		if(hours == null) {
			hours = utilityFunctions.getOpeningHours(utilityFunctions.getNextOpeningTime(utilityFunctions.getDayStart(fromTime, 1), this.fromToByDays), this.fromToByDays);
		}
		if(fromTime.getTime() >= hours[1].getTime()) {
			hours = utilityFunctions.getOpeningHours(utilityFunctions.getNextOpeningTime(utilityFunctions.getDayStart(hours[1], 1), this.fromToByDays), this.fromToByDays);
		}

		int itemIdx = utilityFunctions.hasAppointmentAtTime(fromTime, this.scheduleItems);
		if(itemIdx > -1) {
			if(fromTime.getTime() >= hours[0].getTime()) {
				if(this.scheduleItems.get(itemIdx).getStartTime().getTime() <= fromTime.getTime()) {
					if(this.scheduleItems.get(itemIdx).getStartTime().getTime() > hours[0].getTime() && this.scheduleItems.get(itemIdx).getStartTime().getTime() < hours[1].getTime()) {
						if(utilityFunctions.hasAppointmentAtTime(this.scheduleItems.get(itemIdx).getEndTime(), this.scheduleItems) < 0) {
							if(this.scheduleItems.get(itemIdx).getEndTime().getTime() >= hours[0].getTime() && this.scheduleItems.get(itemIdx).getEndTime().getTime() < hours[1].getTime()) {
								return this.scheduleItems.get(itemIdx).getEndTime();
							}
						}
					}
				}
			}
			else if(fromTime.getTime() < hours[0].getTime()) {
				if(this.scheduleItems.get(itemIdx).getEndTime().getTime() <= hours[0].getTime()) {
					if(utilityFunctions.hasAppointmentAtTime(hours[0], this.scheduleItems) < 0) {
						return hours[0];
					}
				}
			}
		}
		else if(itemIdx < 0) {
			itemIdx = utilityFunctions.getFirstItemAfter(fromTime, this.scheduleItems, true);
			if(fromTime.getTime() >= hours[0].getTime() && fromTime.getTime() < hours[1].getTime()) {
				return fromTime;
			}
			if(itemIdx > -1) {
				if(fromTime.getTime() < hours[0].getTime() && this.scheduleItems.get(itemIdx).getStartTime().getTime() > hours[0].getTime()) {
					if(utilityFunctions.hasAppointmentAtTime(hours[0], this.scheduleItems) < 0) {
						return hours[0];
					}
				}
				else if(this.scheduleItems.get(itemIdx).getEndTime().getTime() <= hours[0].getTime()) {
					if(utilityFunctions.hasAppointmentAtTime(hours[0], this.scheduleItems) < 0) {
						return hours[0];
					}
				}
			}
		}

		if(itemIdx < 0) {
			if(fromTime.getTime() < hours[0].getTime()) {
				if(utilityFunctions.hasAppointmentAtTime(hours[0], this.scheduleItems) < 0) {
					return hours[0];
				}
			}
			else if(fromTime.getTime() < hours[1].getTime()) {
				if(utilityFunctions.hasAppointmentAtTime(fromTime, this.scheduleItems) < 0) {
					return fromTime;
				}
			}
			else if(fromTime.getTime() >= hours[1].getTime()) {
				hours = utilityFunctions.getOpeningHours(utilityFunctions.getNextOpeningTime(utilityFunctions.getDayStart(fromTime, 1), this.fromToByDays), this.fromToByDays);
				if(utilityFunctions.hasAppointmentAtTime(hours[0], this.scheduleItems) < 0) {
					return hours[0];
				}
			}
		}

		ScheduleItem item;
		while(utilityFunctions.moreItems(itemIdx, this.scheduleItems) > 0) {
			item = this.scheduleItems.get(itemIdx);
			final ScheduleItem nextItem = this.scheduleItems.get(itemIdx + 1);
			hours = utilityFunctions.getOpeningHours(item.getEndTime(), this.fromToByDays);
			if(item.getEndTime().getTime() < nextItem.getStartTime().getTime()) {
				if(hours != null) {
					if(item.getEndTime().getTime() >= hours[0].getTime() && item.getEndTime().getTime() < hours[1].getTime()) {
						if(utilityFunctions.hasAppointmentAtTime(item.getEndTime(), this.scheduleItems) < 0) {
							return item.getEndTime();
						}
					}
					else {
						hours = utilityFunctions.getOpeningHours(nextItem.getStartTime(), this.fromToByDays);
						if(hours != null) {
							if(nextItem.getStartTime().getTime() > hours[0].getTime()) {
								if(hours[0].getTime() >= fromTime.getTime()) {
									if(utilityFunctions.hasAppointmentAtTime(hours[0], this.scheduleItems) < 0) {
										return hours[0];
									}
								}
							}
						}
					}
				}
			}
			itemIdx++;
		}//End while(utilityFunctions.moreItems(itemIdx, this.scheduleItems) > 0)

		item = this.scheduleItems.get(scheduleItemsSize - 1);
		hours = utilityFunctions.getOpeningHours(item.getStartTime(), this.fromToByDays);
		if(hours != null) {
			if(item.getStartTime().getTime() > hours[0].getTime()) {
				if(utilityFunctions.hasAppointmentAtTime(hours[0], this.scheduleItems) < 0) {
					if(hours[0].getTime() >= fromTime.getTime()) {
						return hours[0];
					}
				}
			}
		}
		hours = utilityFunctions.getOpeningHours(utilityFunctions.getNextOpeningTime(utilityFunctions.getDayStart(item.getEndTime(), 0), this.fromToByDays), this.fromToByDays);
		if(hours == null) {
			hours = utilityFunctions.getOpeningHours(utilityFunctions.getNextOpeningTime(utilityFunctions.getDayStart(item.getEndTime(), 1), this.fromToByDays), this.fromToByDays);
		}
		if(item.getEndTime().getTime() < hours[0].getTime()) {
			return hours[0];
		}
		else if(item.getEndTime().getTime() >= hours[0].getTime() && item.getEndTime().getTime() < hours[1].getTime()) {
			return item.getEndTime();
		}
		else if(item.getEndTime().getTime() >= hours[1].getTime()) {
			hours = utilityFunctions.getOpeningHours(utilityFunctions.getNextOpeningTime(utilityFunctions.getDayStart(item.getEndTime(), 0), this.fromToByDays), this.fromToByDays);
			if(item.getEndTime().getTime() < hours[0].getTime()) {
				return hours[0];
			}
			else if(item.getEndTime().getTime() >= hours[0].getTime() && item.getEndTime().getTime() < hours[1].getTime()) {
				return item.getEndTime();
			}
			else if(item.getEndTime().getTime() >= hours[1].getTime()) {
				hours = utilityFunctions.getOpeningHours(utilityFunctions.getNextOpeningTime(utilityFunctions.getDayStart(item.getEndTime(), 1), this.fromToByDays), this.fromToByDays);
				return hours[0];
			}
		}

		return null;

	}//End public Date getNextAvailable(final Date fromTime, final List<ScheduleItem> scheduleItems, final List<FromToByDay> fromToByDays)


	/* Gets all gaps between appointments. */
	public List<ScheduleItem> getGaps(final Date fromTime, final List<ScheduleItem> scheduleItems) {

		final List<ScheduleItem> returnVal = new ArrayList<>();

		if(isSortedFlag == false) {
			this.scheduleItems.clear();
			this.scheduleItems.addAll(scheduleItems);
			this.scheduleItems = utilityFunctions.getSortedScheduleItems(START, true, scheduleItems);
			isSortedFlag = true;
		}

		ScheduleItem item;
		ScheduleItem nextItem = null;
		final int scheduleItemsSize = this.scheduleItems.size();
		int startIdx = 0;

		for(int i = 0; i < scheduleItemsSize; i++) {
			if(this.scheduleItems.get(i).getEndTime().getTime() >= fromTime.getTime()) {
				startIdx = i;
				break;
			}
		}

		final int endIdx = scheduleItemsSize - 1;
		int itemIterator = startIdx;

		while(itemIterator < endIdx) {
			item = this.scheduleItems.get(itemIterator);
			nextItem = this.scheduleItems.get(itemIterator + 1);
			if(item.getEndTime().getTime() < nextItem.getStartTime().getTime()) {
				returnVal.add(new ScheduleItem(item.getEndTime(), nextItem.getStartTime()));
			}
			itemIterator++;
		}

		if(scheduleItemsSize > 1) {
			gapsEndAnchor = nextItem.getEndTime();
		}
		else if(scheduleItemsSize == 1) {
			gapsEndAnchor = this.scheduleItems.get(endIdx).getEndTime();
		}
		else {
			returnVal.removeAll(returnVal);
			gapsEndAnchor = fromTime;
		}

		return returnVal;

	}//End public List<ScheduleItem> getGaps(final Date fromTime, final List<ScheduleItem> scheduleItems, final List<FromToByDay> fromToByDays)


	/* Gets all available slots that are between opening times and closing times where appointments don't exist */
	public List<ScheduleItem> getAvailableTimes(final Date fromTime, final Date toTime, final List<ScheduleItem> scheduleItems, final List<FromToByDay> fromToByDays) {

		final List<ScheduleItem> returnVal = new ArrayList<>();
		this.fromToByDays = utilityFunctions.set247(fromToByDays);
		Date[] hours;
		Date tomorrow;
		int hasItems;
		Date gapStart = null;
		Date gapEnd = null;

		hours = utilityFunctions.getOpeningHours(fromTime, this.fromToByDays);
		if(hours == null) {
			hours = utilityFunctions.getOpeningHours(utilityFunctions.getNextOpeningTime(new Date(fromTime.getTime() + MILLISECONDS_PER_DAY), this.fromToByDays), this.fromToByDays);
		}

		List<ScheduleItem> hourItems = utilityFunctions.getItemsForDay(hours[0], this.scheduleItems);
		int hourItemsSize = hourItems.size();

		do {

			if(hourItemsSize > 1) {

				Date startAnchor = null;
				Date endAnchor = null;

				gapEnd = hourItems.get(0).getStartTime();

				if(hours[0].getTime() > fromTime.getTime()) {
					startAnchor = hours[0];
				}
				else if(hours[0].getTime() <= fromTime.getTime()) {
					startAnchor = fromTime;
				}

				if(hours[1].getTime() > toTime.getTime()) {
					if(gapEnd.getTime() >= toTime.getTime()) {
						endAnchor = toTime;
					}
					else if(gapEnd.getTime() < toTime.getTime()) {
						endAnchor = gapEnd;
					}
				}
				else if(hours[1].getTime() <= toTime.getTime()) {
					if(gapEnd.getTime() >= hours[1].getTime()) {
						endAnchor = hours[1];
					}
					else if(gapEnd.getTime() < hours[1].getTime()) {
						endAnchor = gapEnd;
					}
				}

				if(startAnchor.getTime() < endAnchor.getTime()) {returnVal.add(new ScheduleItem(startAnchor, endAnchor));}

				hasItems = 0;

				while(utilityFunctions.moreItems(hasItems, hourItems) > 0) {

					gapStart = hourItems.get(hasItems).getEndTime();
					gapEnd = hourItems.get(hasItems + 1).getStartTime();

					if(gapStart.getTime() < gapEnd.getTime()) {

						startAnchor = null;
						endAnchor = null;

						if(hours[0].getTime() > fromTime.getTime()) {
							if(gapStart.getTime() >= hours[0].getTime()) {
								startAnchor = gapStart;
							}
							else if(gapStart.getTime() < hours[0].getTime()) {
								startAnchor = hours[0];
							}
						}
						else if(hours[0].getTime() <= fromTime.getTime()) {
							if(gapStart.getTime() >= fromTime.getTime()) {
								startAnchor = gapStart;
							}
							else if(gapStart.getTime() < fromTime.getTime()) {
								startAnchor = fromTime;
							}
						}//End if(hours[0].getTime() > fromTime.getTime())

						if(hours[1].getTime() > toTime.getTime()) {
							if(gapEnd.getTime() >= toTime.getTime()) {
								endAnchor = toTime;
							}
							else if(gapEnd.getTime() < toTime.getTime()) {
								endAnchor = gapEnd;
							}
						}
						else if(hours[1].getTime() <= toTime.getTime()) {
							if(gapEnd.getTime() >= hours[1].getTime()) {
								endAnchor = hours[1];
							}
							else if(gapEnd.getTime() < hours[1].getTime()) {
								endAnchor = gapEnd;
							}
						}//End if(hours[1].getTime() > toTime.getTime())

						if(startAnchor.getTime() < endAnchor.getTime()) {returnVal.add(new ScheduleItem(startAnchor, endAnchor));}

					}//End if(gapStart.getTime() < gapEnd.getTime())

					hasItems++;
				}//End while(utilityFunctions.moreItems(hasItems, hourItems) > 0)

				gapStart = hourItems.get(hasItems).getEndTime();
				if(hours[0].getTime() >= fromTime.getTime()) {
					if(gapStart.getTime() >= hours[0].getTime()) {
						startAnchor = gapStart;
					}
					else if(gapStart.getTime() < hours[0].getTime()) {
						startAnchor = hours[0];
					}
				}
				else if(hours[0].getTime() < fromTime.getTime()) {
					if(gapStart.getTime() >= fromTime.getTime()) {
						startAnchor = gapStart;
					}
					else if(gapStart.getTime() < fromTime.getTime()) {
						startAnchor = fromTime;
					}
				}

				if(hours[1].getTime() >= toTime.getTime()) {
					endAnchor = toTime;
				}
				else if(hours[1].getTime() < toTime.getTime()) {
					endAnchor = hours[1];
				}

				if(startAnchor.getTime() < endAnchor.getTime()) {returnVal.add(new ScheduleItem(startAnchor, endAnchor));}

			}
			else if(hourItemsSize > 0) {

				if(hours[0].getTime() > fromTime.getTime()) {
					if(hourItems.get(0).getStartTime().getTime() > hours[0].getTime()) {
						gapStart = hours[0];
						if(hourItems.get(0).getStartTime().getTime() <= toTime.getTime()) {
							gapEnd = hourItems.get(0).getStartTime();
						}
						else if(hourItems.get(0).getStartTime().getTime() > toTime.getTime()) {
							gapEnd = toTime;
						}
						if(gapStart.getTime() < gapEnd.getTime()) {returnVal.add(new ScheduleItem(gapStart, gapEnd));}
					}
				}
				else if(hours[0].getTime() <= fromTime.getTime()) {

					if(hourItems.get(0).getStartTime().getTime() > fromTime.getTime()) {
						gapStart = fromTime;
						if(hourItems.get(0).getStartTime().getTime() <= toTime.getTime()) {
							gapEnd = hourItems.get(0).getStartTime();
						}
						else if(hourItems.get(0).getStartTime().getTime() > toTime.getTime()) {
							gapEnd = toTime;
						}
						if(gapStart.getTime() < gapEnd.getTime()) {returnVal.add(new ScheduleItem(gapStart, gapEnd));}
					}
				}//End if(hours[0].getTime() > fromTime.getTime())

				if(hours[1].getTime() > toTime.getTime()) {
					if(hourItems.get(0).getEndTime().getTime() < toTime.getTime()) {
						if(hourItems.get(0).getEndTime().getTime() >= fromTime.getTime()) {
							gapStart = hourItems.get(0).getEndTime();
						}
						else if(hourItems.get(0).getEndTime().getTime() < fromTime.getTime()) {
							gapStart = fromTime;
						}
						gapEnd = toTime;
						if(gapStart.getTime() < gapEnd.getTime()) {returnVal.add(new ScheduleItem(gapStart, gapEnd));}
					}
				}
				else if(hours[1].getTime() <= toTime.getTime()) {
					if(hourItems.get(0).getEndTime().getTime() < hours[1].getTime()) {
						if(hourItems.get(0).getEndTime().getTime() >= fromTime.getTime()) {
							gapStart = hourItems.get(0).getEndTime();
						}
						else if(hourItems.get(0).getEndTime().getTime() < fromTime.getTime()) {
							gapStart = fromTime;
						}
						gapEnd = hours[1];
						if(gapStart.getTime() < gapEnd.getTime()) {returnVal.add(new ScheduleItem(gapStart, gapEnd));}
					}
				}//End if(hours[1].getTime() > toTime.getTime())
			}
			else if(hourItemsSize < 1) {

				if(hours[0].getTime() > fromTime.getTime()) {
					gapStart = hours[0];
				}
				else if(hours[0].getTime() <= fromTime.getTime()) {
					gapStart = fromTime;
				}

				if(hours[1].getTime() > toTime.getTime()) {
					gapEnd = toTime;
				}
				else if(hours[1].getTime() <= toTime.getTime()) {
					gapEnd = hours[1];
				}

				if(gapStart.getTime() < gapEnd.getTime()) {returnVal.add(new ScheduleItem(gapStart, gapEnd));}

			}//End if(hourItemsSize > 1)

			tomorrow = utilityFunctions.getDayStart(hours[0], 1);
			hours = utilityFunctions.getOpeningHours(utilityFunctions.getNextOpeningTime(tomorrow, this.fromToByDays), this.fromToByDays);
			hourItems = utilityFunctions.getItemsForDay(hours[0], this.scheduleItems);
			hourItemsSize = hourItems.size();

		} while(tomorrow.getTime() < toTime.getTime());

		return returnVal;

	}//End private List<ScheduleItem> getAvailableTimes(final Date fromTime, final Date toTime, final List<ScheduleItem> gapItems, final List<FromToByDay> fromToByDays)

}