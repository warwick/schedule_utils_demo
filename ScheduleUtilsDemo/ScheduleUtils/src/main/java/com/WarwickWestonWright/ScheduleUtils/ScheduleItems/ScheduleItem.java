/*
LICENSE. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
Copyright (c) 2018, Warwick Weston Wright All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
/*
Class Usage 1:
1. Use the default constructor ScheduleItem()
2. Then Call setStartTime(final Date startTime)
3. the Call one of:
	3.1 setDuration(final long duration) Which in turn sets the the EndTime
	3.2 setEndTime(final Date endTime) Which in turn sets the duration


Class Usage 2:
Call constructor: ScheduleItem(final Date startTime, final long duration) This constructor will internally set the EndTime

Class Usage 3:
Call constructor: ScheduleItem(final Date startTime, final Date endTime) This constructor will internally set the duration
*/
package com.WarwickWestonWright.ScheduleUtils.ScheduleItems;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class ScheduleItem implements Parcelable {
	private Date startTime;
	private long duration;
	private Date endTime;

	public ScheduleItem() {}

	public ScheduleItem(final Date startTime, final long duration) {
		this.startTime = startTime;
		setDuration(duration);
	}

	public ScheduleItem(final Date startTime, final Date endTime) {
		this.startTime = startTime;
		setEndTime(endTime);
	}

	//Getters
	public Date getStartTime() {return this.startTime;}
	public long getDuration() {return this.duration;}
	public Date getEndTime() {return this.endTime;}

	//Setters
	public void setStartTime(final Date startTime) {this.startTime = startTime;}

	public void setDuration(final long duration) {
		this.duration = duration;
		this.endTime = new Date(startTime.getTime() + duration);
	}

	public void setEndTime(final Date endTime) {
		this.endTime = endTime;
		this.duration = endTime.getTime() - startTime.getTime();
	}

	//ADS Generated Code:
	protected ScheduleItem(Parcel in) {duration = in.readLong();}

	public static final Creator<ScheduleItem> CREATOR = new Creator<ScheduleItem>() {
		@Override
		public ScheduleItem createFromParcel(Parcel in) {return new ScheduleItem(in);}
		@Override
		public ScheduleItem[] newArray(int size) {return new ScheduleItem[size];}
	};

	@Override
	public int describeContents() {return 0;}
	@Override
	public void writeToParcel(Parcel dest, int flags) {dest.writeLong(duration);}
}