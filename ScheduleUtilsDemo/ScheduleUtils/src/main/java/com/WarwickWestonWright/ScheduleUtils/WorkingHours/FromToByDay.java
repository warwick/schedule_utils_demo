/*
LICENSE. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
Copyright (c) 2018, Warwick Weston Wright All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
/*
Class Usage:
Simple data holder class for holding hours of business. This class is meant to be used as an array or
7 items (one for each day of the week). From and to time is expressed as HH:MM:SS, HH:MM or HH
*/
package com.WarwickWestonWright.ScheduleUtils.WorkingHours;

import android.os.Parcel;
import android.os.Parcelable;

public class FromToByDay implements Parcelable {

	private String from;
	private String to;
	private int day;

	public FromToByDay() {}

	public FromToByDay(final String from, final String to, final int day) {
		//Time Format = "HH:MM:SS" 09:48:22;
		this.from = from;
		this.to = to;
		this.day = day;
	}

	//Getters
	public String getFrom() {return this.from;}
	public String getTo() {return this.to;}
	public int getDay() {return this.day;}

	//Setters
	public void setFrom(final String from) {this.from = from;}
	public void setTo(final String to) {this.to = to;}
	public void setDay(int day) {this.day = day;}

	protected FromToByDay(Parcel in) {
		from = in.readString();
		to = in.readString();
		day = in.readInt();
	}

	public static final Creator<FromToByDay> CREATOR = new Creator<FromToByDay>() {
		@Override
		public FromToByDay createFromParcel(Parcel in) {return new FromToByDay(in);}
		@Override
		public FromToByDay[] newArray(int size) {return new FromToByDay[size];}
	};

	@Override
	public int describeContents() {return 0;}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(from);
		dest.writeString(to);
		dest.writeInt(day);
	}
}