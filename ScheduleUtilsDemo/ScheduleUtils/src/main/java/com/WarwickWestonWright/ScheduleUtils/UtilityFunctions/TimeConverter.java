package com.WarwickWestonWright.ScheduleUtils.UtilityFunctions;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public class TimeConverter {

	private final SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public Date getDateFromMySqlFormat(String dateTime) {
		final String[] dateTimeParts = dateTime.split(" ");
		final String[] dateParts = dateTimeParts[0].split("-");
		final String[] timeParts = dateTimeParts[1].split(":");
		int hour = 0;
		int minute = 0;
		int second = 0;
		int milli = 0;

		int month = 8;
		String monthString;
		switch (timeParts.length) {
			case 2:
				hour = Integer.parseInt(timeParts[0]);
				minute = Integer.parseInt(timeParts[1]);
				second = 0;
				milli = 0;
				break;
			case 3:
				hour = Integer.parseInt(timeParts[0]);
				minute = Integer.parseInt(timeParts[1]);
				second = Integer.parseInt(timeParts[2]);
				milli = 0;
				break;
			case 1:
				hour = Integer.parseInt(timeParts[0]);
				minute = 0;
				second = 0;
				milli = 0;
				break;
			case 4:
				hour = Integer.parseInt(timeParts[0]);
				minute = Integer.parseInt(timeParts[1]);
				second = Integer.parseInt(timeParts[2]);
				milli = Integer.parseInt(timeParts[3]);
				break;
			default:
				hour = Integer.parseInt(timeParts[0]);
				minute = Integer.parseInt(timeParts[1]);
				second = Integer.parseInt(timeParts[2]);
				milli = Integer.parseInt(timeParts[3]);
				break;
		}
		return new Date(new GregorianCalendar(Integer.parseInt(dateParts[0]), Integer.parseInt(dateParts[1]) - 1, Integer.parseInt(dateParts[2]), hour, minute, second).getTime().getTime() + milli);
	}

	public String getMysqlFormatFromDate(Date dateTime) {
		return sdf.format(dateTime.getTime());
	}

	public String getFriendlyDuration(long duration) {
		final long hourDuration = duration / 3600000L;
		final long minutesDuration = duration - (hourDuration * 3600000L);
		return Long.toString(hourDuration) + ":" + String.format("%02d", minutesDuration / 60000L);
	}

}