/*
LICENSE. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
Copyright (c) 2018, Warwick Weston Wright All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
/*
Class Overview
This class validates values in the FromToByDay object which contains a FromTime: 09:00, a ToTime 17:00 and a day of the week indicated with values of 0 - 6 (Sunday - Saturday)

Class Usage: Simply construct class with default constructor and call the setFromToByDay(FromToByDay fromToByDay) method then call one of the two validate overloads.
The validateTimePart(String timePattern) overload is only needed if you want to enforce a different time format to the default.
Calling the validateTimePart() overload will check to the default time format (Hour with 1 or 2 digits followed by a ':' followed by 2 digits)
If you construct with the Validators(FromToByDay fromToByDay) overload then you don't need to call setFromToByDay(FromToByDay fromToByDay).

Validation rules are as follows:
From and To times may not be null.
If From time is empty so too must be To time. Empty From/To times are reserved for a closed for business scenario.
FromTime may not be greater than ToTime
From/To Times must be in the following format: 09:00 or 9:00

Advisory note: Should your usage require working hours that go past midnight such as for a night club, then you need to set a closing time of one minute before midnight and
an opening time for midnight for the following day.
ie Saturday From 20:00 to 23:59 - Sunday 00:00 to 05:00
*/
package com.WarwickWestonWright.ScheduleUtils.WorkingHours;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validators {

	private Pattern pattern;
	private Matcher matcher;
	private FromToByDay fromToByDay;
	private String dayOfWeek;
	private final String invalidDOW = "Invalid Day of Week. Valid Values are 1 - 7 (Sunday to Saturday)";
	private String timePattern = "^\\d{1,2}:{1}\\d{1,2}$";

	public Validators() {}
	public Validators(final FromToByDay fromToByDay) {this.fromToByDay = fromToByDay;}
	public void setFromToByDay(final FromToByDay fromToByDay) {this.fromToByDay = fromToByDay;}

	public void setTimePattern(String timePattern) {this.timePattern = timePattern;}
	public String getTimePattern() {return this.timePattern;}

	public String validateTimePart(final String timePattern) {
		this.timePattern = timePattern;
		return validateTimePart();
	}//End public String validateTimePart(final String timePattern)

	//ToDo: Check that all 7 days exists and not just that the list is 7 items in size.
	public String validateTimePart() {

		String returnVal = "";

		if(fromToByDay.getDay() < 1 || fromToByDay.getDay() > 7) {
			return invalidDOW;
		}
		//Day constants are consistent with the GregorianCalendar object default
		switch(fromToByDay.getDay()) {
			case 1:  dayOfWeek = "Sunday";
				break;
			case 2:  dayOfWeek = "Monday";
				break;
			case 3:  dayOfWeek = "Tuesday";
				break;
			case 4:  dayOfWeek = "Wednesday";
				break;
			case 5:  dayOfWeek = "Thursday";
				break;
			case 6:  dayOfWeek = "Friday";
				break;
			case 7:  dayOfWeek = "Saturday";
				break;
			default: dayOfWeek = invalidDOW;
				break;
		}

		pattern = Pattern.compile(timePattern);
		int totalFromMinutes = 0;
		int totalToMinutes = 0;
		if(fromToByDay.getFrom() != null) {
			matcher = pattern.matcher(fromToByDay.getFrom());
			if(matcher.find(0) == false && fromToByDay.getFrom().isEmpty() == false) {
				returnVal += "Invalid From Value: " + fromToByDay.getFrom() + " For: " + dayOfWeek;
			}
			else if(matcher.find(0) == true && fromToByDay.getFrom().isEmpty() == false) {
				String[] fromParts = fromToByDay.getFrom().split(":");
				int hourPart = Integer.parseInt(fromParts[0]);
				int minutePart = Integer.parseInt(fromParts[1]);

				if(hourPart > 23 || hourPart < 0) {
					returnVal += "Invalid From Value: " + fromToByDay.getFrom() + " For: " + dayOfWeek + " Hour value must be between 0 and 23 (inclusive)";
				}
				else if(hourPart < 24 && hourPart > -1) {
					totalFromMinutes += hourPart * 60;
				}

				if(minutePart > 59 || minutePart < 0) {
					returnVal += "Invalid From Value: " + fromToByDay.getFrom() + " For: " + dayOfWeek + " Minute value must be between 0 and 59 (inclusive)";
				}
				else if(minutePart < 24 && minutePart > -1) {
					totalFromMinutes += minutePart;
				}

			}//End if(matcher.find(0) == false && fromToByDay.getFrom().isEmpty() == false)

		}
		else if(fromToByDay.getFrom() == null) {
			returnVal += "From value cannot be null: " + dayOfWeek;
		}//End if(fromToByDay.getFrom() != null)

		//ToDo make open ended so that time can include seconds and milliseconds (not priority)
		if(fromToByDay.getTo() != null) {
			matcher = pattern.matcher(fromToByDay.getTo());
			if(matcher.find(0) == false && fromToByDay.getTo().isEmpty() == false) {
				returnVal += "\nInvalid To Value: " + fromToByDay.getTo() + " For: " + dayOfWeek;
			}
			else if(matcher.find(0) == true && fromToByDay.getTo().isEmpty() == false) {
				String[] toParts = fromToByDay.getTo().split(":");
				int hourPart = Integer.parseInt(toParts[0]);
				int minutePart = Integer.parseInt(toParts[1]);

				if(hourPart > 23 || hourPart < 0) {
					returnVal += "\nInvalid To Value: " + fromToByDay.getTo() + " For: " + dayOfWeek + " Hour value must be between 0 and 23 (inclusive)";
				}
				else if(hourPart < 24 && hourPart > -1) {
					totalToMinutes += hourPart * 60;
				}

				if(minutePart > 59 || minutePart < 0) {
					returnVal += "\nInvalid To Value: " + fromToByDay.getTo() + " For: " + dayOfWeek + " Minute value must be between 0 and 59 (inclusive)";
				}
				else if(minutePart < 24 && minutePart > -1) {
					totalToMinutes += minutePart;
				}

			}
		}
		else if(fromToByDay.getTo() == null) {
			returnVal += "To value cannot be null: " + dayOfWeek;
		}//End if(fromToByDay.getTo() != null)

		if(fromToByDay.getFrom() != null && fromToByDay.getTo() != null) {
			if((fromToByDay.getFrom().isEmpty() == true && fromToByDay.getTo().isEmpty() == false) || (fromToByDay.getFrom().isEmpty() == false && fromToByDay.getTo().isEmpty() == true)) {
				returnVal += "If From value is empty so mut be the To value." + dayOfWeek;
			}
		}

		if(totalToMinutes < totalFromMinutes) {
			returnVal += "From value may not be after To value." + dayOfWeek;
		}

		if(returnVal.equals("")) {
			return "Success";
		}
		else {
			return returnVal;
		}

	}//End public String validateTimePart()


	public boolean validateMySQLFormat(final String subject) {
		final String dateTimeStr = subject.trim();
		final String dateTimePatternStr = "^\\d{4}-\\d{2}-\\d{2}\\s{1}\\d{2}:{1}\\d{2}:{1}\\d{2}$";
		final Matcher dateTimeMatcher;
		final Pattern dateTimePattern = Pattern.compile(dateTimePatternStr);
		dateTimeMatcher = dateTimePattern.matcher(dateTimeStr);
		return dateTimeMatcher.find();
	}

}