/*
LICENSE. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
Copyright (c) 2018, Warwick Weston Wright All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
/*
Class Usage 1:
Call default constructor ItemActionsValidator()
followed by setScheduleItems(final List<ScheduleItem> scheduleItems);
or
Class Usage 2:
Construct with ItemActionsValidator(final List<ScheduleItem> scheduleItems)

After construction call int validate(ScheduleItem scheduleItem)
This method will check for double booking conflicts with any schedule items parsed by method setScheduleItems or
constructor ItemActionsValidator(final List<ScheduleItem> scheduleItems).
validate(ScheduleItem scheduleItem) returns -1 on success or the index of the conflicting item. If scheduleItem conflicts
with more than one item the index returned will be associated with the scheduleItem start time.
*/
package com.WarwickWestonWright.ScheduleUtils.ScheduleItems;

import com.WarwickWestonWright.ScheduleUtils.UtilityFunctions.UtilityFunctions;

import java.util.List;

public class ItemActionsValidator {

	private List<ScheduleItem> scheduleItems;
	private int scheduleItemsSize;
	private UtilityFunctions utilityFunctions;
	public ItemActionsValidator() {this.utilityFunctions = new UtilityFunctions();}

	public ItemActionsValidator(final List<ScheduleItem> scheduleItems) {
		this.scheduleItems = scheduleItems;
		this.scheduleItemsSize = scheduleItems.size();
		this.utilityFunctions = new UtilityFunctions();
	}

	public void setScheduleItems(final List<ScheduleItem> scheduleItems) {
		this.scheduleItems = scheduleItems;
		this.scheduleItemsSize = scheduleItems.size();
	}

	/* Checks for conflict for adding/editing schedule items parse -1 for add or the index of the scheduleItems array that you are editing.
	On success returns -1 or the index of the conflicting item. Note Schedule items must be sorted ascending for ths method to work. */
	public int validate(final ScheduleItem scheduleItem, final int excludeForEdit) {
		int idx;

		if(scheduleItemsSize < 1) {return -1;}

		if(excludeForEdit < 0) {
			//Condition for adding
			final int firstAfter = utilityFunctions.getFirstItemAfter(scheduleItem.getStartTime(), this.scheduleItems, true);
			if(firstAfter > -1) {
				if(scheduleItem.getEndTime().getTime() > this.scheduleItems.get(firstAfter).getStartTime().getTime()) {
					return firstAfter;
				}
			}

			for(idx = 0; idx < scheduleItemsSize; idx++) {
				if(scheduleItem.getStartTime().getTime() >= scheduleItems.get(idx).getStartTime().getTime() && scheduleItem.getStartTime().getTime() < scheduleItems.get(idx).getEndTime().getTime()) {
					return idx;
				}
			}//End for(idx = 0; idx < scheduleItemsSize; idx++)
		}
		else if(excludeForEdit > -1) {
			//Condition for editing
			final int firstAfter = utilityFunctions.getFirstItemAfter(scheduleItem.getStartTime(), this.scheduleItems, true);
			if(firstAfter > -1) {
				if(firstAfter != excludeForEdit) {
					if(scheduleItem.getEndTime().getTime() > this.scheduleItems.get(firstAfter).getStartTime().getTime()) {
						return firstAfter;
					}
				}//End if(firstAfter != excludeForEdit)
			}

			for(idx = 0; idx < scheduleItemsSize; idx++) {
				if(idx != excludeForEdit) {
					if(scheduleItem.getStartTime().getTime() >= scheduleItems.get(idx).getStartTime().getTime() && scheduleItem.getStartTime().getTime() < scheduleItems.get(idx).getEndTime().getTime()) {
						return idx;
					}
				}//End if(idx != excludeForEdit)
			}//End for(idx = 0; idx < scheduleItemsSize; idx++)

		}//End if(excludeForEdit == -1)

		return -1;
	}
}