/*
Though package 'com.WarwickWestonWright.ScheduleUtils' is protected by open source, the source code in this package (com.WarwickWestonWright.ScheduleUtilsDemo) is free.
Should you wish to commission the developer you can contact him at: warwickwestonwright@gmail.com
*/
package com.WarwickWestonWright.ScheduleUtilsDemo;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.WarwickWestonWright.ScheduleUtils.ScheduleItems.ScheduleItem;
import com.WarwickWestonWright.ScheduleUtils.UtilityFunctions.TimeConverter;
import com.WarwickWestonWright.ScheduleUtils.WorkingHours.FromToByDay;
import com.WarwickWestonWright.ScheduleUtils.WorkingHours.Validators;
import com.WarwickWestonWright.ScheduleUtilsDemo.Dialogs.SetNowFragment;
import com.WarwickWestonWright.ScheduleUtilsDemo.room.ScheduleHoursDB;
import com.WarwickWestonWright.ScheduleUtilsDemo.room.WorkingHours;

import java.lang.ref.WeakReference;
import java.util.Date;
import java.util.List;

import static android.util.TypedValue.COMPLEX_UNIT_DIP;
import static com.WarwickWestonWright.ScheduleUtils.LibConstants.MILLISECONDS_PER_DAY;
import static com.WarwickWestonWright.ScheduleUtilsDemo.AppConstants.SCHEMA_INIT;
import static com.WarwickWestonWright.ScheduleUtilsDemo.AppConstants.TAB_HOURS;

public class MainActivity extends AppCompatActivity implements
	Runnable,
	HoursFragment.IHoursFragment,
	ItemsFragment.IItemsFragment,
	View.OnClickListener,
	SetNowFragment.ISetNowFragment {

	private WorkingHours hours;
	private ScheduleHoursDB scheduleHoursDB;
	private List<FromToByDay> fromToByDays;
	private Validators validators;
	private int dbAction = SCHEMA_INIT;
	private Thread dbThread;
	private String toastMsg;
	private final MainActivityHandler mainActivityHandler = new MainActivityHandler(this);
	private static int defaultTab = TAB_HOURS;//ToDo set default tab
	private HoursFragment hoursFragment;
	private ItemsFragment itemsFragment;
	private SetNowFragment setNowFragment;
	private Button btnSetNow;
	private Date testTime;

	private SharedPreferences sp;
	private final TimeConverter timeConverter = new TimeConverter();
	private Button btnHours;
	private Button btnItems;
	private InputMethodManager imm;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_activity);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		sp = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);

		if(sp.getBoolean("isSetup", false) == false) {
			sp.edit().putBoolean("isSetup", true).apply();
			final Date from = new Date();
			final Date to = new Date(from.getTime() + MILLISECONDS_PER_DAY);
			sp.edit().putString("from", timeConverter.getMysqlFormatFromDate(from)).apply();
			sp.edit().putString("to", timeConverter.getMysqlFormatFromDate(to)).apply();
			sp.edit().putString("setNow", "").apply();
			sp.edit().putBoolean("isSetup", true).apply();
		}

		createCustomActionBar();
		btnHours = findViewById(R.id.btnHours);
		btnItems = findViewById(R.id.btnItems);

		btnHours.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				attachHoursFragment();
			}
		});

		btnItems.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				itemsFragment = new ItemsFragment();
				getSupportFragmentManager().beginTransaction().replace(R.id.fragContainer, itemsFragment, "ItemsFragment").commit();
			}
		});

		scheduleHoursDB = ScheduleHoursDB.getDatabase(getApplicationContext());
		hours = ViewModelProviders.of(this).get(WorkingHours.class);
		if(dbThread == null) {dbThread = new Thread(this);}
		dbAction = SCHEMA_INIT;
		dbThread.start();
	}//End protected void onCreate(Bundle savedInstanceState)


	private void attachHoursFragment() {
		hoursFragment = new HoursFragment();
		Bundle args = new Bundle();

		if(testTime != null) {
			args.putString("testTime", timeConverter.getMysqlFormatFromDate(testTime));
		}
		else if(testTime == null) {
			args.putString("testTime", "");
		}

		hoursFragment.setArguments(args);
		getSupportFragmentManager().beginTransaction().replace(R.id.fragContainer, hoursFragment, "HoursFragment").commit();
	}//End private void attachHoursFragment()


	@Override
	public void run() {
		if(dbAction == SCHEMA_INIT) {
			if(scheduleHoursDB.hoursDao().getAllHours().size() == 0) {
				toastMsg = "Initial Hours Hss Been Setup";
				setDefaultHours();
			}
			else {
				toastMsg = "Hours Already Initialized";
			}
		}
		mainActivityHandler.sendEmptyMessage(dbAction);
	}


	private void createCustomActionBar() {
		this.getSupportActionBar().setDisplayShowCustomEnabled(true);
		this.getSupportActionBar().setDisplayShowTitleEnabled(false);
		LayoutInflater inflator = LayoutInflater.from(this);
		View v = inflator.inflate(R.layout.title_bar, null);
		btnSetNow = v.findViewById(R.id.btnSetNow);
		Typeface tf = Typeface.createFromAsset(getAssets(),"Lobster-Regular.ttf");
		((TextView) v.findViewById(R.id.lblActionBarTitle)).setTypeface(tf);
		((TextView) v.findViewById(R.id.lblActionBarTitle)).setTypeface(tf);
		btnSetNow.setTypeface(tf);
		btnSetNow.setOnClickListener(this);

		if(sp.getString("setNow", timeConverter.getMysqlFormatFromDate(new Date())).equals("")) {
			btnSetNow.setTextSize(COMPLEX_UNIT_DIP, 24);
			btnSetNow.setText(getString(R.string.btn_set_now));
		}
		else {
			final String testTimeStr = sp.getString("setNow", timeConverter.getMysqlFormatFromDate(new Date()));
			final String[] testTimeParts = testTimeStr.split(" ");
			btnSetNow.setTextSize(COMPLEX_UNIT_DIP, 18);
			btnSetNow.setText(testTimeParts[0] + "\n" + testTimeParts[1]);
		}

		this.getSupportActionBar().setCustomView(v);
		this.getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#212121")));
	}//End private void createCustomActionBar()


	@Override
	public void onClick(View v) {
		setNowFragment = new SetNowFragment();
		setNowFragment.setTargetFragment(getSupportFragmentManager().findFragmentByTag("HoursFragment"), 0);
		setNowFragment.setCancelable(false);
		setNowFragment.show(getSupportFragmentManager(), "SetNowFragment");
	}//End public void onClick(View v)


	@Override
	public void setNowFragmentCB(final Date testTime) {
		this.testTime = testTime;
		SetNowFragment setNowFragment = (SetNowFragment) getSupportFragmentManager().findFragmentByTag("SetNowFragment");
		if(setNowFragment != null && setNowFragment.isAdded() == true) {
			setNowFragment.dismiss();
		}

		if(testTime != null) {
			final String testTimeStr = timeConverter.getMysqlFormatFromDate(testTime);
			final String[] testTimeParts = testTimeStr.split(" ");
			sp.edit().putString("setNow", testTimeStr).apply();
			btnSetNow.setTextSize(COMPLEX_UNIT_DIP, 18);
			btnSetNow.setText(testTimeParts[0] + "\n" + testTimeParts[1]);
		}
		else if(testTime == null) {
			sp.edit().putString("setNow", "").apply();
			btnSetNow.setTextSize(COMPLEX_UNIT_DIP, 24);
			btnSetNow.setText(getString(R.string.btn_set_now));
		}//End if(testTime != null)

	}//End public void setNowFragmentCB(final Date testTime)


	private void setDefaultHours() {

		//SUNDAY Default Hours
		hours.setDay(WorkingHours.SUNDAY);
		hours.setFrom("");
		hours.setTo("");
		scheduleHoursDB.hoursDao().insertHours(hours);

		//MONDAY Default Hours
		hours.setDay(WorkingHours.MONDAY);
		hours.setFrom("09:00");
		hours.setTo("17:00");
		scheduleHoursDB.hoursDao().insertHours(hours);

		//TUESDAY Default Hours
		hours.setDay(WorkingHours.TUESDAY);
		hours.setFrom("09:00");
		hours.setTo("17:00");
		scheduleHoursDB.hoursDao().insertHours(hours);

		//WEDNESDAY Default Hours
		hours.setDay(WorkingHours.WEDNESDAY);
		hours.setFrom("09:00");
		hours.setTo("17:00");
		scheduleHoursDB.hoursDao().insertHours(hours);

		//THURSDAY Default Hours
		hours.setDay(WorkingHours.THURSDAY);
		hours.setFrom("09:00");
		hours.setTo("17:00");
		scheduleHoursDB.hoursDao().insertHours(hours);

		//FRIDAY Default Hours
		hours.setDay(WorkingHours.FRIDAY);
		hours.setFrom("08:00");
		hours.setTo("18:00");
		scheduleHoursDB.hoursDao().insertHours(hours);

		//SATURDAY Default Hours
		hours.setDay(WorkingHours.SATURDAY);
		hours.setFrom("10:00");
		hours.setTo("16:00");
		scheduleHoursDB.hoursDao().insertHours(hours);

	}

	@Override
	public void hoursFragmentCB(List<FromToByDay> fromToByDays) {
    	/*Dev Note The validation code here is redundant because the HoursFragment validates the input. This validation only code exists to
    	demonstrate class usage. You can uncomment the code below to test the Validators object*/
		this.fromToByDays = fromToByDays;
		//fromToByDays.get(FromToByDay.THURSDAY).setFrom("09:00");//Uncomment to test
		//fromToByDays.get(FromToByDay.THURSDAY).setTo("17:00");//Uncomment to test
		final int fromToByDaysSize = fromToByDays.size();
		boolean validationResult = true;
		String validateResult;
		for(int i = 0; i < fromToByDaysSize; i++) {
			validators = new Validators(fromToByDays.get(i));
			validateResult = validators.validateTimePart();
			if(validateResult.equals("Success") == false) {
				Toast.makeText(this, validateResult, Toast.LENGTH_LONG).show();
				validationResult = false;
				break;
			}
		}

		if(validationResult == true) {
			Toast.makeText(this, "Success", Toast.LENGTH_LONG).show();
		}

	}//End public void hoursFragmentCB(List<FromToByDay> fromToByDays)


	@Override
	public void itemsFragmentCB(List<ScheduleItem> scheduleItems) {}

	private static class MainActivityHandler extends Handler {
		private final WeakReference<MainActivity> mainActivityWeakReference;
		public MainActivityHandler(MainActivity mainFragment) {
			mainActivityWeakReference = new WeakReference<>(mainFragment);
		}

		@Override
		public void handleMessage(Message msg) {

			MainActivity mainActivity = mainActivityWeakReference.get();

			if(mainActivity != null) {

				mainActivity.dbThread = null;

				if(mainActivity.dbAction == SCHEMA_INIT) {
					if(mainActivity.hoursFragment == null) {mainActivity.hoursFragment = new HoursFragment();}
					mainActivity.getSupportFragmentManager().beginTransaction().replace(R.id.fragContainer, mainActivity.hoursFragment, "HoursFragment").commit();
				}

			}//End if(mainActivity != null)

		}

	}

}