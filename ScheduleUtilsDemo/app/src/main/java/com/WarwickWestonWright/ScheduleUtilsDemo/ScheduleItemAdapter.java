package com.WarwickWestonWright.ScheduleUtilsDemo;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.WarwickWestonWright.ScheduleUtils.ScheduleItems.ScheduleItem;
import com.WarwickWestonWright.ScheduleUtils.UtilityFunctions.TimeConverter;
import com.WarwickWestonWright.ScheduleUtilsDemo.ScheduleItemListFragment.IScheduleItemListFragment;

import java.util.List;

public class ScheduleItemAdapter extends RecyclerView.Adapter<ScheduleItemAdapter.ViewHolder> {

	private final List<ScheduleItem> scheduleItems;
	private final IScheduleItemListFragment iScheduleItemListFragment;
	private TimeConverter timeConverter;

	public ScheduleItemAdapter(List<ScheduleItem> scheduleItems, IScheduleItemListFragment iScheduleItemListFragment) {
		this.scheduleItems = scheduleItems;
		this.iScheduleItemListFragment = iScheduleItemListFragment;
		this.timeConverter = new TimeConverter();
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.schedule_item, parent, false);
		return new ViewHolder(view);
	}

	@Override
	public void onBindViewHolder(final ViewHolder holder, int position) {
		holder.scheduleItem = scheduleItems.get(position);
		holder.lblFrom.setText(timeConverter.getMysqlFormatFromDate(scheduleItems.get(position).getStartTime()));
		holder.lblDuration.setText(timeConverter.getFriendlyDuration(scheduleItems.get(position).getDuration()));
		holder.lblTo.setText(timeConverter.getMysqlFormatFromDate(scheduleItems.get(position).getEndTime()));
		holder.mView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(null != iScheduleItemListFragment) {
					iScheduleItemListFragment.scheduleItemListFragmentCB(holder.scheduleItem);
				}
			}
		});
	}

	@Override
	public int getItemCount() {
		return scheduleItems.size();
	}

	public class ViewHolder extends RecyclerView.ViewHolder {
		public final View mView;
		public final TextView lblFrom;
		public final TextView lblDuration;
		public final TextView lblTo;
		public ScheduleItem scheduleItem;

		public ViewHolder(View view) {
			super(view);
			mView = view;
			lblFrom = view.findViewById(R.id.lblFrom);
			lblDuration = view.findViewById(R.id.lblDuration);
			lblTo = view.findViewById(R.id.lblTo);
		}

		@Override
		public String toString() {
			return super.toString() + " '" + lblDuration.getText() + "'";
		}
	}
}