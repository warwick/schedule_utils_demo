/*
Though package 'com.WarwickWestonWright.ScheduleUtils' is protected by open source, the source code in this package (com.WarwickWestonWright.ScheduleUtilsDemo) is free.
Should you wish to commission the developer you can contact him at: warwickwestonwright@gmail.com
*/
package com.WarwickWestonWright.ScheduleUtilsDemo;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.WarwickWestonWright.ScheduleUtils.AvailabilityCalculator;
import com.WarwickWestonWright.ScheduleUtils.LibConstants;
import com.WarwickWestonWright.ScheduleUtils.ScheduleItems.ScheduleItem;
import com.WarwickWestonWright.ScheduleUtils.UtilityFunctions.TimeConverter;
import com.WarwickWestonWright.ScheduleUtils.WorkingHours.FromToByDay;
import com.WarwickWestonWright.ScheduleUtilsDemo.room.Appointment;
import com.WarwickWestonWright.ScheduleUtilsDemo.room.ScheduleHoursDB;
import com.WarwickWestonWright.ScheduleUtilsDemo.room.WorkingHours;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HoursFragment extends Fragment implements
	Runnable {

	public interface IHoursFragment {void hoursFragmentCB(List<FromToByDay> fromToByDays);}
	private IHoursFragment iHoursFragment;

	private final HoursFragmentHandler hoursFragmentHandler = new HoursFragmentHandler(this);
	private View rootView;
	private Button btnUpdateHours;
	private Button btnHoursCalculate;

	private ArrayAdapter<CharSequence> adapter;

	private Spinner spnrSundayFrom;
	private Spinner spnrSundayTo;
	private Spinner spnrMondayFrom;
	private Spinner spnrMondayTo;
	private Spinner spnrTuesdayFrom;
	private Spinner spnrTuesdayTo;
	private Spinner spnrWednesdayFrom;
	private Spinner spnrWednesdayTo;
	private Spinner spnrThursdayFrom;
	private Spinner spnrThursdayTo;
	private Spinner spnrFridayFrom;
	private Spinner spnrFridayTo;
	private Spinner spnrSaturdayFrom;
	private Spinner spnrSaturdayTo;

	private Thread fetchHoursThread;

	private String validationMsg;
	private ScheduleHoursDB scheduleHoursDB;
	private AvailabilityCalculator avCalc;
	private final TimeConverter timeConverter = new TimeConverter();
	private static final int GET_ALL_HOURS = 1;
	private static final int SET_ALL_HOURS = 2;
	private int formAction = 1;
	private List<Appointment> appointments = new ArrayList<>();
	private List<WorkingHours> workingHours;
	private List<ScheduleItem> scheduleItems = new ArrayList<>();
	private List<FromToByDay> fromToByDays;
	private final HoursFragment context = this;
	private SharedPreferences sp;

	public HoursFragment() {}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		scheduleHoursDB = ScheduleHoursDB.getDatabase(getActivity().getApplicationContext());
		sp = getActivity().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.hours_fragment, container, false);
		if(fromToByDays == null) {fromToByDays = new ArrayList<>();}
		adapter = ArrayAdapter.createFromResource(getActivity(), R.array.arr_hours_selector, R.layout.spinner_view);
		adapter.setDropDownViewResource(R.layout.spinner_view);
		spnrSundayFrom = rootView.findViewById(R.id.spnrSundayFrom);
		spnrSundayFrom.setAdapter(adapter);
		spnrSundayTo = rootView.findViewById(R.id.spnrSundayTo);
		spnrSundayTo.setAdapter(adapter);
		spnrMondayFrom = rootView.findViewById(R.id.spnrMondayFrom);
		spnrMondayFrom.setAdapter(adapter);
		spnrMondayTo = rootView.findViewById(R.id.spnrMondayTo);
		spnrMondayTo.setAdapter(adapter);
		spnrTuesdayFrom = rootView.findViewById(R.id.spnrTuesdayFrom);
		spnrTuesdayFrom.setAdapter(adapter);
		spnrTuesdayTo = rootView.findViewById(R.id.spnrTuesdayTo);
		spnrTuesdayTo.setAdapter(adapter);
		spnrWednesdayFrom = rootView.findViewById(R.id.spnrWednesdayFrom);
		spnrWednesdayFrom.setAdapter(adapter);
		spnrWednesdayTo = rootView.findViewById(R.id.spnrWednesdayTo);
		spnrWednesdayTo.setAdapter(adapter);
		spnrThursdayFrom = rootView.findViewById(R.id.spnrThursdayFrom);
		spnrThursdayFrom.setAdapter(adapter);
		spnrThursdayTo = rootView.findViewById(R.id.spnrThursdayTo);
		spnrThursdayTo.setAdapter(adapter);
		spnrFridayFrom = rootView.findViewById(R.id.spnrFridayFrom);
		spnrFridayFrom.setAdapter(adapter);
		spnrFridayTo = rootView.findViewById(R.id.spnrFridayTo);
		spnrFridayTo.setAdapter(adapter);
		spnrSaturdayFrom = rootView.findViewById(R.id.spnrSaturdayFrom);
		spnrSaturdayFrom.setAdapter(adapter);
		spnrSaturdayTo = rootView.findViewById(R.id.spnrSaturdayTo);
		spnrSaturdayTo.setAdapter(adapter);

		formAction = GET_ALL_HOURS;
		if(fetchHoursThread == null) {fetchHoursThread = new Thread(context);}
		fetchHoursThread.start();

		spnrSundayFrom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				if(position == 0) {
					spnrSundayTo.setSelection(0);
				}
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {}
		});

		spnrSundayTo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				if(position == 0) {
					spnrSundayFrom.setSelection(0);
				}
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {}
		});

		spnrMondayFrom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				if(position == 0) {
					spnrMondayTo.setSelection(0);
				}
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {}
		});

		spnrMondayTo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				if(position == 0) {
					spnrMondayFrom.setSelection(0);
				}
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {}
		});

		spnrTuesdayFrom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				if(position == 0) {
					spnrTuesdayTo.setSelection(0);
				}
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {}
		});

		spnrTuesdayTo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				if(position == 0) {
					spnrTuesdayFrom.setSelection(0);
				}
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {}
		});

		spnrWednesdayFrom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				if(position == 0) {
					spnrWednesdayTo.setSelection(0);
				}
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {}
		});

		spnrWednesdayTo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				if(position == 0) {
					spnrWednesdayFrom.setSelection(0);
				}
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {}
		});

		spnrThursdayFrom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				if(position == 0) {
					spnrThursdayTo.setSelection(0);
				}
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {}
		});

		spnrThursdayTo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				if(position == 0) {
					spnrThursdayFrom.setSelection(0);
				}
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {}
		});

		spnrFridayFrom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				if(position == 0) {
					spnrFridayTo.setSelection(0);
				}
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {}
		});

		spnrFridayTo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				if(position == 0) {
					spnrFridayFrom.setSelection(0);
				}
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {}
		});

		spnrSaturdayFrom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				if(position == 0) {
					spnrSaturdayTo.setSelection(0);
				}
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {}
		});

		spnrSaturdayTo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				if(position == 0) {
					spnrSaturdayFrom.setSelection(0);
				}
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {}
		});

		btnUpdateHours = rootView.findViewById(R.id.btnUpdateHours);
		btnHoursCalculate = rootView.findViewById(R.id.btnHoursCalculate);

		btnUpdateHours.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				final boolean isValid = validateHours();
				if(isValid == true) {
					formAction = SET_ALL_HOURS;
					populateFromToByDays(fromToByDays);
					if(fetchHoursThread == null) {fetchHoursThread = new Thread(context);}
					fetchHoursThread.start();
					iHoursFragment.hoursFragmentCB(fromToByDays);
				}
				else if(isValid == false) {
					showMessage(validationMsg);
				}

			}
		});

		btnHoursCalculate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				avCalc = new AvailabilityCalculator(scheduleItems, fromToByDays);
				/* Test Lines of Code to test all utility functions
				scheduleItems = utilityFunctions.getSortedScheduleItems(UtilityFunctions.START, false, scheduleItems);
				Execute 2 lines below to test reordering line above.
				setAllAppointments();
				attachListFragment();
				final GregorianCalendar toTime = new GregorianCalendar(2018, 9, 9, 12, 0);
				List<ScheduleItem> availSlots = avCalc.getAvailableSlots(calendar.getTime(), toTime.getTime(), scheduleItems, fromToByDays);
				final boolean io = utilityFunctions.isOpen(calendar.getTime(), fromToByDays);
				final boolean io = avCalc.isOpen(calendar.getTime(), null);//null for 24/7
				final int hasAppointment = utilityFunctions.hasAppointmentAtTime(calendar.getTime(), scheduleItems);
				final boolean iaa = utilityFunctions.isAvailableAt(calendar.getTime(), scheduleItems, fromToByDays);
				final Date tomorrow = utilityFunctions.getDayStart(calendar.getTime(), 1);
				final Date[] openingHours = utilityFunctions.getOpeningHours(calendar.getTime(), fromToByDays);
				final Date nextOpen = utilityFunctions.getNextOpeningTime(calendar.getTime(), null);
				final int hasItem = utilityFunctions.hasItemForToday(calendar.getTime(), scheduleItems);
				final Date nextAvailable = avCalc.getNextAvailable(calendar.getTime(), scheduleItems, fromToByDays);
				*/

				Date nextAvailable;
				if(sp.getString("setNow", "").equals("")) {
					nextAvailable = avCalc.getNextAvailable(new Date(), scheduleItems, fromToByDays);
				}
				else {
					nextAvailable = avCalc.getNextAvailable(timeConverter.getDateFromMySqlFormat(sp.getString("setNow", timeConverter.getMysqlFormatFromDate(new Date()))), scheduleItems, fromToByDays);
				}
				showMessage("Next Available at:\n" + timeConverter.getMysqlFormatFromDate(nextAvailable));
			}
		});

		return rootView;

	}//End public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)

	private boolean validateHours() {
		String errMsg = "";

		if(spnrSundayFrom.getSelectedItemPosition() > spnrSundayTo.getSelectedItemPosition()) {
			errMsg += "\nSunday From Time after Sunday To Time";
		}

		if(spnrMondayFrom.getSelectedItemPosition() > spnrMondayTo.getSelectedItemPosition()) {
			errMsg += "\nMonday From Time after Monday To Time";
		}

		if(spnrTuesdayFrom.getSelectedItemPosition() > spnrTuesdayTo.getSelectedItemPosition()) {
			errMsg += "\nTuesday From Time after Tuesday To Time";
		}

		if(spnrWednesdayFrom.getSelectedItemPosition() > spnrWednesdayTo.getSelectedItemPosition()) {
			errMsg += "\nWednesday From Time after Wednesday To Time";
		}

		if(spnrThursdayFrom.getSelectedItemPosition() > spnrThursdayTo.getSelectedItemPosition()) {
			errMsg += "\nThursday From Time after Thursday To Time";
		}

		if(spnrFridayFrom.getSelectedItemPosition() > spnrFridayTo.getSelectedItemPosition()) {
			errMsg += "\nFriday From Time after Friday To Time";
		}

		if(spnrSaturdayFrom.getSelectedItemPosition() > spnrSaturdayTo.getSelectedItemPosition()) {
			errMsg += "\nSaturday From Time after Saturday To Time";
		}

		this.validationMsg = errMsg;
		if(errMsg.equals("")) {
			return true;
		}
		else {
			return false;
		}

	}//End private boolean validateHours()

	private void populateFromToByDays(final List<FromToByDay> fromToByDays) {

		fromToByDays.clear();
		fromToByDays.add(new FromToByDay(adapter.getItem(spnrSundayFrom.getSelectedItemPosition()).toString(),
			adapter.getItem(spnrSundayTo.getSelectedItemPosition()).toString(), LibConstants.SUNDAY));

		fromToByDays.add(new FromToByDay(adapter.getItem(spnrMondayFrom.getSelectedItemPosition()).toString(),
			adapter.getItem(spnrMondayTo.getSelectedItemPosition()).toString(), LibConstants.MONDAY));

		fromToByDays.add(new FromToByDay(adapter.getItem(spnrTuesdayFrom.getSelectedItemPosition()).toString(),
			adapter.getItem(spnrTuesdayTo.getSelectedItemPosition()).toString(), LibConstants.TUESDAY));

		fromToByDays.add(new FromToByDay(adapter.getItem(spnrWednesdayFrom.getSelectedItemPosition()).toString(),
			adapter.getItem(spnrWednesdayTo.getSelectedItemPosition()).toString(), LibConstants.WEDNESDAY));

		fromToByDays.add(new FromToByDay(adapter.getItem(spnrThursdayFrom.getSelectedItemPosition()).toString(),
			adapter.getItem(spnrThursdayTo.getSelectedItemPosition()).toString(), LibConstants.THURSDAY));

		fromToByDays.add(new FromToByDay(adapter.getItem(spnrFridayFrom.getSelectedItemPosition()).toString(),
			adapter.getItem(spnrFridayTo.getSelectedItemPosition()).toString(), LibConstants.FRIDAY));

		fromToByDays.add(new FromToByDay(adapter.getItem(spnrSaturdayFrom.getSelectedItemPosition()).toString(),
			adapter.getItem(spnrSaturdayTo.getSelectedItemPosition()).toString(), LibConstants.SATURDAY));

	}//End private void populateFromToByDays(final List<FromToByDay> fromToByDays)


	private void showMessage(final String toastMsg) {
		Toast.makeText(getActivity(), toastMsg, Toast.LENGTH_SHORT).show();
	}


	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if(context instanceof IHoursFragment) {
			iHoursFragment = (IHoursFragment) context;
		}
		else {
			throw new RuntimeException(context.toString() + " must implement IHoursFragment");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		iHoursFragment = null;
	}

	@Override
	public void run() {
		if(formAction == GET_ALL_HOURS) {
			getHours();
			getAllAppointments();
		}
		else if(formAction == SET_ALL_HOURS) {
			updateHours();
		}
		hoursFragmentHandler.sendEmptyMessage(0);
	}

	private void getHours() {
		workingHours = scheduleHoursDB.hoursDao().getAllHours();
	}//End private void getHours()


	private void getAllAppointments() {
		appointments = scheduleHoursDB.appointmentsDAO().getAllAppointments();
		scheduleItems.clear();
		final int aptSize = appointments.size();
		for(int i = 0; i < aptSize; i++) {
			scheduleItems.add(new ScheduleItem(timeConverter.getDateFromMySqlFormat(appointments.get(i).getStart()), appointments.get(i).getDuration()));
		}
	}//End private void getAllAppointments()


	private void upDateSpinner() {

		fromToByDays.clear();
		for(WorkingHours item: workingHours) {

			final int fromPos = adapter.getPosition(workingHours.get(item.getDay() - 1).getFrom());
			final int toPos = adapter.getPosition(workingHours.get(item.getDay() - 1).getTo());
			fromToByDays.add(new FromToByDay(item.getFrom(), item.getTo(), item.getDay()));

			switch(item.getDay()) {
				case 1:
					spnrSundayFrom.setSelection(fromPos);
					spnrSundayTo.setSelection(toPos);
					break;
				case 2:
					spnrMondayFrom.setSelection(fromPos);
					spnrMondayTo.setSelection(toPos);
					break;
				case 3:
					spnrTuesdayFrom.setSelection(fromPos);
					spnrTuesdayTo.setSelection(toPos);
					break;
				case 4:
					spnrWednesdayFrom.setSelection(fromPos);
					spnrWednesdayTo.setSelection(toPos);
					break;
				case 5:
					spnrThursdayFrom.setSelection(fromPos);
					spnrThursdayTo.setSelection(toPos);
					break;
				case 6:
					spnrFridayFrom.setSelection(fromPos);
					spnrFridayTo.setSelection(toPos);
					break;
				case 7:
					spnrSaturdayFrom.setSelection(fromPos);
					spnrSaturdayTo.setSelection(toPos);
					break;
			}

		}//End for(WorkingHours item: workingHours)

	}//End private void upDateSpinner()


	private void updateHours() {

		final WorkingHours wh = new WorkingHours();
		//Sunday
		wh.setDay(WorkingHours.SUNDAY);
		wh.setFrom(adapter.getItem(spnrSundayFrom.getSelectedItemPosition()).toString());
		wh.setTo(adapter.getItem(spnrSundayTo.getSelectedItemPosition()).toString());
		scheduleHoursDB.hoursDao().upDateHours(wh);

		//Monday
		wh.setDay(WorkingHours.MONDAY);
		wh.setFrom(adapter.getItem(spnrMondayFrom.getSelectedItemPosition()).toString());
		wh.setTo(adapter.getItem(spnrMondayTo.getSelectedItemPosition()).toString());
		scheduleHoursDB.hoursDao().upDateHours(wh);

		//Tuesday
		wh.setDay(WorkingHours.TUESDAY);
		wh.setFrom(adapter.getItem(spnrTuesdayFrom.getSelectedItemPosition()).toString());
		wh.setTo(adapter.getItem(spnrTuesdayTo.getSelectedItemPosition()).toString());
		scheduleHoursDB.hoursDao().upDateHours(wh);

		//Wednesday
		wh.setDay(WorkingHours.WEDNESDAY);
		wh.setFrom(adapter.getItem(spnrWednesdayFrom.getSelectedItemPosition()).toString());
		wh.setTo(adapter.getItem(spnrWednesdayTo.getSelectedItemPosition()).toString());
		scheduleHoursDB.hoursDao().upDateHours(wh);

		//Thursday
		wh.setDay(WorkingHours.THURSDAY);
		wh.setFrom(adapter.getItem(spnrThursdayFrom.getSelectedItemPosition()).toString());
		wh.setTo(adapter.getItem(spnrThursdayTo.getSelectedItemPosition()).toString());
		scheduleHoursDB.hoursDao().upDateHours(wh);

		//Friday
		wh.setDay(WorkingHours.FRIDAY);
		wh.setFrom(adapter.getItem(spnrFridayFrom.getSelectedItemPosition()).toString());
		wh.setTo(adapter.getItem(spnrFridayTo.getSelectedItemPosition()).toString());
		scheduleHoursDB.hoursDao().upDateHours(wh);

		//Saturday
		wh.setDay(WorkingHours.SATURDAY);
		wh.setFrom(adapter.getItem(spnrSaturdayFrom.getSelectedItemPosition()).toString());
		wh.setTo(adapter.getItem(spnrSaturdayTo.getSelectedItemPosition()).toString());
		scheduleHoursDB.hoursDao().upDateHours(wh);

	}//End private void updateHours()


	private static class HoursFragmentHandler extends Handler {
		private final WeakReference<HoursFragment> hoursFragmentWeakReference;
		public HoursFragmentHandler(HoursFragment hoursFragment) {
			hoursFragmentWeakReference = new WeakReference<>(hoursFragment);
		}

		@Override
		public void handleMessage(Message msg) {

			HoursFragment hoursFragment = hoursFragmentWeakReference.get();

			if(hoursFragment != null) {
				hoursFragment.fetchHoursThread = null;
				if(hoursFragment.formAction == GET_ALL_HOURS) {
					hoursFragment.upDateSpinner();
				}
			}//End if(hoursFragment != null)

		}

	}

}