/*
Though package 'com.WarwickWestonWright.ScheduleUtils' is protected by open source, the source code in this package (com.WarwickWestonWright.ScheduleUtilsDemo) is free.
Should you wish to commission the developer you can contact him at: warwickwestonwright@gmail.com
*/
package com.WarwickWestonWright.ScheduleUtilsDemo.Dialogs;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.WarwickWestonWright.ScheduleUtilsDemo.R;

public class AlertDlgFrag extends DialogFragment implements View.OnClickListener {

	public interface IAlertDlgFrag {void alertDlgFragCb(String buttonsLabel);}

	private IAlertDlgFrag iAlertDlgFrag;

	private String title;
	private String msg;
	private String[] buttonLabels;

	private Button[] buttons;
	private TextView lblTitle;
	private TextView lblMsg;
	private LinearLayout lLayoutButtonsContainer;
	private View rootView;

	public AlertDlgFrag() {}

	/*
	public static AlertDlgFrag newInstance(String param1, String param2) {
		AlertDlgFrag fragment = new AlertDlgFrag();
		Bundle args = new Bundle();
		args.putString(ARG_PARAM1, param1);
		args.putString(ARG_PARAM2, param2);
		fragment.setArguments(args);
		return fragment;
	}
	*/

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if(iAlertDlgFrag == null) {iAlertDlgFrag = (IAlertDlgFrag) getTargetFragment();}
		if(getArguments() != null) {
			title = getArguments().getString("title", "");
			msg = getArguments().getString("msg", "");
			buttonLabels = getArguments().getStringArray("buttons");
		}
		else {
			title = "";
			msg = "";
			buttonLabels = new String[] {"OK"};
		}
	}

	/*
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {return super.onCreateDialog(savedInstanceState);}
	*/

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.alert_dlg_fragment, container, false);

		lblTitle = rootView.findViewById(R.id.lblTitle);
		lblMsg = rootView.findViewById(R.id.lblMsg);
		lblTitle.setText(title);
		lblMsg.setText(msg);
		lLayoutButtonsContainer = rootView.findViewById(R.id.lLayoutButtonsContainer);
		final int buttonsLen = buttonLabels.length;
		buttons = new Button[buttonLabels.length];
		for(int i = 0; i < buttonsLen; i++) {
			buttons[i] = new Button(getActivity().getBaseContext());
			buttons[i].setTag(buttonLabels[i]);
			buttons[i].setText(buttonLabels[i]);
			buttons[i].setOnClickListener(this);
			buttons[i].setTextColor(getResources().getColor(R.color.colorLightLime));
			buttons[i].setBackgroundColor(getResources().getColor(R.color.darbBg));
			lLayoutButtonsContainer.addView(buttons[i], i);
		}
		return rootView;
	}

	@Override
	public void onClick(View v) {iAlertDlgFrag.alertDlgFragCb((String) v.getTag());}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if(context instanceof IAlertDlgFrag) {
			iAlertDlgFrag = (IAlertDlgFrag) context;
		}
		/* else {throw new RuntimeException(context.toString() + " must implement IAlertDlgFrag");} */
	}

	@Override
	public void onDetach() {
		super.onDetach();
		iAlertDlgFrag = null;
	}

}