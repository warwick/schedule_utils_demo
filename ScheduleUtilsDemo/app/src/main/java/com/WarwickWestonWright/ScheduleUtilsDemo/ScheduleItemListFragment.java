package com.WarwickWestonWright.ScheduleUtilsDemo;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.WarwickWestonWright.ScheduleUtils.ScheduleItems.ScheduleItem;

import java.util.ArrayList;
import java.util.List;

public class ScheduleItemListFragment extends Fragment {

	private int mColumnCount = 1;

	//ToDo Lose this class and move this interface to SlotsFragment
	public interface IScheduleItemListFragment {
		void scheduleItemListFragmentCB(final ScheduleItem scheduleItem);
	}
	private IScheduleItemListFragment iScheduleItemListFragment;

	private View rootView;
	private List<ScheduleItem> availSlots;

	public ScheduleItemListFragment() {}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if(iScheduleItemListFragment == null) {
			iScheduleItemListFragment = (IScheduleItemListFragment) getTargetFragment();
		}

		if(getArguments() != null) {
			availSlots = getArguments().getParcelableArrayList("availSlots");
		}
		else {
			availSlots = new ArrayList<>();
		}

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.schedule_item_list, container, false);

		if(view instanceof RecyclerView) {
			Context context = getTargetFragment().getContext();
			RecyclerView recyclerView = (RecyclerView) view;
			if(mColumnCount <= 1) {
				recyclerView.setLayoutManager(new LinearLayoutManager(context));
			}
			else {
				recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
			}
			recyclerView.setAdapter(new ScheduleItemAdapter(new ArrayList<ScheduleItem>(), iScheduleItemListFragment));
		}

		return view;

	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if(context instanceof IScheduleItemListFragment) {
			iScheduleItemListFragment = (IScheduleItemListFragment) getTargetFragment();
		}
		else {
			throw new RuntimeException(context.toString() + " must implement IScheduleItemListFragment");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		iScheduleItemListFragment = null;
	}
}