/*
Though package 'com.WarwickWestonWright.ScheduleUtils' is protected by open source, the source code in this package (com.WarwickWestonWright.ScheduleUtilsDemo) is free.
Should you wish to commission the developer you can contact him at: warwickwestonwright@gmail.com
*/
package com.WarwickWestonWright.ScheduleUtilsDemo.room;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

@Database(entities = {Appointment.class, WorkingHours.class}, version = 1)
public abstract class ScheduleHoursDB extends RoomDatabase {

	public abstract AppointmentDAO appointmentsDAO();
	public abstract HoursDao hoursDao();
	private static ScheduleHoursDB INSTANCE;

	public static ScheduleHoursDB getDatabase(final Context context) {

		if(INSTANCE == null) {
			synchronized(ScheduleHoursDB.class) {
				if(INSTANCE == null) {
					INSTANCE = Room.databaseBuilder(context.getApplicationContext(), ScheduleHoursDB.class, "scheduleDB").build();
				}
			}
		}
		return INSTANCE;
	}

}