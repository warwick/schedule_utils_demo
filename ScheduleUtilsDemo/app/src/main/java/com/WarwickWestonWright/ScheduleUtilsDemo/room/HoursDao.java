/*
Though package 'com.WarwickWestonWright.ScheduleUtils' is protected by open source, the source code in this package (com.WarwickWestonWright.ScheduleUtilsDemo) is free.
Should you wish to commission the developer you can contact him at: warwickwestonwright@gmail.com
*/
package com.WarwickWestonWright.ScheduleUtilsDemo.room;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface HoursDao {

	@Query("DELETE FROM workingHours")
	void deleteAllHours();

	@Insert(onConflict = OnConflictStrategy.REPLACE)
	void insertHours(WorkingHours workingHours);

	@Query("SELECT * from workingHours ORDER BY day ASC")
	List<WorkingHours> getAllHours();

	@Query("SELECT * from workingHours WHERE day = :day ORDER BY day ASC")
	WorkingHours getHoursForDay(int day);

	@Update
	public void upDateHours(WorkingHours workingHours);

}