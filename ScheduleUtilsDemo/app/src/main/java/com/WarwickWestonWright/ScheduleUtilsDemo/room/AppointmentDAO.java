/*
Though package 'com.WarwickWestonWright.ScheduleUtils' is protected by open source, the source code in this package (com.WarwickWestonWright.ScheduleUtilsDemo) is free.
Should you wish to commission the developer you can contact him at: warwickwestonwright@gmail.com
*/
package com.WarwickWestonWright.ScheduleUtilsDemo.room;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface AppointmentDAO {

	@Query("DELETE FROM appointments")
	void deleteAllAppointments();

	@Query("DELETE FROM appointments WHERE id = :id")
	void deleteAppointment(final long id);

	@Insert(onConflict = OnConflictStrategy.REPLACE)
	void insertItems(Appointment appointment);

	@Query("SELECT * from appointments ORDER BY id ASC")
	List<Appointment> getAllAppointments();

	@Update
	public void upDateAppointment(Appointment appointment);

}