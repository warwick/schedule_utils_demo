/*
Though package 'com.WarwickWestonWright.ScheduleUtils' is protected by open source, the source code in this package (com.WarwickWestonWright.ScheduleUtilsDemo) is free.
Should you wish to commission the developer you can contact him at: warwickwestonwright@gmail.com
*/
package com.WarwickWestonWright.ScheduleUtilsDemo.room;

import android.arch.lifecycle.ViewModel;
import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "workingHours")
public class WorkingHours extends ViewModel {

	//Day constants are consistent with the GregorianCalendar object
	public static final int SUNDAY = 1;
	public static final int MONDAY = 2;
	public static final int TUESDAY = 3;
	public static final int WEDNESDAY = 4;
	public static final int THURSDAY = 5;
	public static final int FRIDAY = 6;
	public static final int SATURDAY = 7;

	@PrimaryKey
	@NonNull
	@ColumnInfo(name = "day")
	private int day;
	private String from;
	private String to;

	//Getters
	public int getDay() {return this.day;}
	public String getFrom() {return this.from;}
	public String getTo() {return this.to;}

	//Setters
	public void setDay(int day) {this.day = day;}
	public void setFrom(String from) {this.from = from;}
	public void setTo(String to) {this.to = to;}

}