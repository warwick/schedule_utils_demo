/*
Though package 'com.WarwickWestonWright.ScheduleUtils' is protected by open source, the source code in this package (com.WarwickWestonWright.ScheduleUtilsDemo) is free.
Should you wish to commission the developer you can contact him at: warwickwestonwright@gmail.com
*/
package com.WarwickWestonWright.ScheduleUtilsDemo.room;

import android.arch.lifecycle.ViewModel;
import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

@Entity(tableName = "appointments")
public class Appointment extends ViewModel implements Parcelable {

	public Appointment(final long id, final String start, final long duration, final String end) {
		this.id = id;
		this.start = start;
		this.duration = duration;
		this.end = end;
	}

	@PrimaryKey
	@NonNull
	@ColumnInfo(name = "id")
	private long id;//Id will be the start time in milliseconds
	private String start;
	private long duration;
	private String end;

	//Getters
	public long getId() {return this.id;}
	public String getStart() {return this.start;}
	public long getDuration() {return this.duration;}
	public String getEnd() {return this.end;}

	//Setters
	public void setId(long id) {this.id = id;}
	public void setStart(String start) {this.start = start;}
	public void setDuration(long duration) {this.duration = duration;}
	public void setEnd(String end) {this.end = end;}

	protected Appointment(Parcel in) {
		id = in.readLong();
		start = in.readString();
		duration = in.readLong();
		end = in.readString();
	}
	public static final Creator<Appointment> CREATOR = new Creator<Appointment>() {
		@Override
		public Appointment createFromParcel(Parcel in) {return new Appointment(in);}
		@Override
		public Appointment[] newArray(int size) {return new Appointment[size];}
	};

	@Override
	public int describeContents() {return 0;}
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(id);
		dest.writeString(start);
		dest.writeLong(duration);
		dest.writeString(end);
	}
}