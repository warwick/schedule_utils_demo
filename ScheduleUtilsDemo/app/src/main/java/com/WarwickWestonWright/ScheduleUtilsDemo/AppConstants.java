/*
Though package 'com.WarwickWestonWright.ScheduleUtils' is protected by open source, the source code in this package (com.WarwickWestonWright.ScheduleUtilsDemo) is free.
Should you wish to commission the developer you can contact him at: warwickwestonwright@gmail.com
*/
package com.WarwickWestonWright.ScheduleUtilsDemo;

public class AppConstants {
	public static final int SCHEMA_INIT = 0;
	public static final int HOURS_DELETE = 1;
	public static final int HOURS_INSERT = 2;
	public static final int HOURS_SELECT = 3;
	public static final int HOURS_UPDATE = 4;
	public static final int ITEMS_DELETE = 5;
	public static final int ITEMS_INSERT = 6;
	public static final int ITEMS_SELECT = 7;
	public static final int ITEMS_UPDATE = 8;

	public static final int TAB_HOURS = 0;
	public static final int TAB_ITEMS = 1;

}