package com.WarwickWestonWright.ScheduleUtilsDemo.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.WarwickWestonWright.ScheduleUtils.ScheduleItems.ScheduleItem;
import com.WarwickWestonWright.ScheduleUtilsDemo.R;
import com.WarwickWestonWright.ScheduleUtilsDemo.ScheduleItemAdapter;
import com.WarwickWestonWright.ScheduleUtilsDemo.ScheduleItemListFragment;

import java.util.ArrayList;
import java.util.List;

public class SlotsFragment extends DialogFragment implements
	DialogInterface.OnShowListener,
	ScheduleItemListFragment.IScheduleItemListFragment {

	private View rootView;
	private Button bntCancelSlots;

	private List<ScheduleItem> availSlots;

	private ScheduleItemListFragment scheduleItemListFragment;
	private final ScheduleItemListFragment.IScheduleItemListFragment iScheduleItemListFragment = this;

	public interface ISlotsFragment {
		void slotsFragmentCB();
	}

	private ISlotsFragment iSlotsFragment;

	/*
	public SlotsFragment() {}
	public static SlotsFragment newInstance(String param1, String param2) {
		SlotsFragment fragment = new SlotsFragment();
		Bundle args = new Bundle();
		args.putString(ARG_PARAM1, param1);
		args.putString(ARG_PARAM2, param2);
		fragment.setArguments(args);
		return fragment;
	}
	*/

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if(iSlotsFragment == null) {
			iSlotsFragment = (ISlotsFragment) getTargetFragment();
		}

		if(getArguments() != null) {
			availSlots = getArguments().getParcelableArrayList("availSlots");
		}
		else {
			availSlots = new ArrayList<>();
		}

	}//End public void onCreate(Bundle savedInstanceState)


	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		final Dialog dialog = super.onCreateDialog(savedInstanceState);
		dialog.setOnShowListener(this);
		return dialog;
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.slots_fragment, container, false);
		bntCancelSlots = rootView.findViewById(R.id.bntCancelSlots);
		LinearLayout lLayoutSlotsListContainer = rootView.findViewById(R.id.lLayoutSlotsListContainer);
		scheduleItemListFragment = new ScheduleItemListFragment();
		final Bundle args = new Bundle();
		args.putParcelableArrayList("availSlots", (ArrayList<? extends Parcelable>) availSlots);
		scheduleItemListFragment.setArguments(args);

		View view = inflater.inflate(R.layout.schedule_item_list, container, false);
		if(view instanceof RecyclerView) {
			Context context = getTargetFragment().getContext();
			RecyclerView recyclerView = (RecyclerView) view;
			recyclerView.setLayoutManager(new LinearLayoutManager(context));
			recyclerView.setAdapter(new ScheduleItemAdapter(availSlots, iScheduleItemListFragment));
		}

		lLayoutSlotsListContainer.addView(view);

		bntCancelSlots.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(iSlotsFragment != null) {
					iSlotsFragment.slotsFragmentCB();
				}
			}
		});

		showMessage(Integer.toString(availSlots.size()));
		return rootView;
	}


	@Override
	public void scheduleItemListFragmentCB(final ScheduleItem scheduleItem) {
		String bp = "";
	}


	@Override
	public void onShow(DialogInterface dialog) {}


	private void showMessage(final String toastMsg) {
		Toast.makeText(getActivity(), toastMsg, Toast.LENGTH_SHORT).show();
	}


	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if(context instanceof ISlotsFragment) {
			iSlotsFragment = (ISlotsFragment) context;
		}
	}


	@Override
	public void onDetach() {
		super.onDetach();
		iSlotsFragment = null;
	}

}