package com.WarwickWestonWright.ScheduleUtilsDemo.Dialogs;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.inputmethodservice.Keyboard;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.WarwickWestonWright.ScheduleUtils.ScheduleItems.ScheduleItem;
import com.WarwickWestonWright.ScheduleUtils.UtilityFunctions.TimeConverter;
import com.WarwickWestonWright.ScheduleUtils.WorkingHours.Validators;
import com.WarwickWestonWright.ScheduleUtilsDemo.CustomKeyboardView.FloatingKeyboardView;
import com.WarwickWestonWright.ScheduleUtilsDemo.R;

import java.util.Date;

@TargetApi(21)
public class FromToFragment extends DialogFragment {

	private View rootView;
	private EditText txtFrom;
	private EditText txtTo;
	private Button btnGetSlots;
	private Button btnCancelSlots;

	private final TimeConverter timeConverter = new TimeConverter();
	private Bundle args;
	private String fromStr;
	private String toStr;
	private ScheduleItem scheduleItem;
	private Validators validators;
	private FloatingKeyboardView floatingKeyboardView;

	public interface IFromToFragment {void fromToFragmentCB(final ScheduleItem scheduleItem);}

	private IFromToFragment iFromToFragment;

	public FromToFragment() {}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if(iFromToFragment == null) {
			iFromToFragment = (IFromToFragment) getTargetFragment();
		}
		if((args = getArguments()) != null) {
			scheduleItem = args.getParcelable("fromTo");
			fromStr = timeConverter.getMysqlFormatFromDate(scheduleItem.getStartTime());
			toStr = timeConverter.getMysqlFormatFromDate(scheduleItem.getEndTime());
		}
		else {
			scheduleItem = new ScheduleItem(new Date(), new Date());
			fromStr = timeConverter.getMysqlFormatFromDate(scheduleItem.getStartTime());
			toStr = timeConverter.getMysqlFormatFromDate(scheduleItem.getEndTime());
		}//End public void onCreate(Bundle savedInstanceState)
		validators = new Validators();
	}


	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		final Dialog dialog = new Dialog(getActivity());
		dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
			@Override
			public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
				if(event.getAction() == MotionEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
					if(floatingKeyboardView != null && floatingKeyboardView.isShown()) {
						floatingKeyboardView.hide();
						return true;
					}
					return true;
				}
				else {
					return false;
				}
			}
		});
		return dialog;
	}//End public Dialog onCreateDialog(Bundle savedInstanceState)


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.from_to_fragment, container, false);

		floatingKeyboardView = rootView.findViewById(R.id.keyboardView);
		floatingKeyboardView.setKeyboard(new Keyboard(getActivity(), R.xml.keyboard_date_time));
		floatingKeyboardView.setPreviewEnabled(false);//NOTE Do not show the preview balloons

		txtFrom = rootView.findViewById(R.id.txtFrom);
		txtTo = rootView.findViewById(R.id.txtTo);
		btnGetSlots = rootView.findViewById(R.id.btnGetSlots);
		btnCancelSlots = rootView.findViewById(R.id.btnCancelSlots);
		txtFrom.setText(fromStr.substring(0, fromStr.lastIndexOf(":")));//Trim Seconds off of end.
		txtTo.setText(toStr.substring(0, toStr.lastIndexOf(":")));//Trim Seconds off of end.
		//@TargetApi(21) at the top of this class if for the following 2 lines
		txtFrom.setShowSoftInputOnFocus(false);//Dev note is supported from api 16 ignore this error
		txtTo.setShowSoftInputOnFocus(false);//Dev note is supported from api 16 ignore this error
		floatingKeyboardView.registerEditText(txtFrom);
		floatingKeyboardView.registerEditText(txtTo);

		btnGetSlots.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(iFromToFragment != null) {
					txtFrom.setText(txtFrom.getText().toString().trim());
					txtTo.setText(txtTo.getText().toString().trim());
					final String[] fromTimeParts = txtFrom.getText().toString().split(":");
					final String[] toTimeParts = txtTo.getText().toString().split(":");

					switch(fromTimeParts.length) {
						case 2:
							txtFrom.setText(txtFrom.getText().toString() + ":00");
							break;
						case 1:
							txtFrom.setText(txtFrom.getText().toString() + ":00:00");
							break;
						case 0:
							txtFrom.setText(txtFrom.getText().toString() + ":00:00:00");
							break;
						default:
							break;
					}

					switch(toTimeParts.length) {
						case 2:
							txtTo.setText(txtTo.getText().toString() + ":00");
							break;
						case 1:
							txtTo.setText(txtTo.getText().toString() + ":00:00");
							break;
						case 0:
							txtTo.setText(txtTo.getText().toString() + ":00:00:00");
							break;
						default:
							break;
					}

					txtFrom.setText(txtFrom.getText().toString().replaceAll(":{2,}", ":"));
					txtTo.setText(txtTo.getText().toString().replaceAll(":{2,}", ":"));

					if(validators.validateMySQLFormat(txtFrom.getText().toString()) == true && validators.validateMySQLFormat(txtTo.getText().toString()) == true) {
						scheduleItem = new ScheduleItem(timeConverter.getDateFromMySqlFormat(txtFrom.getText().toString()), timeConverter.getDateFromMySqlFormat(txtTo.getText().toString()));
						iFromToFragment.fromToFragmentCB(scheduleItem);
					}
					else {
						showMessage(getString(R.string.msg_invalid_mysql_format));
					}
				}
			}
		});

		btnCancelSlots.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				getDialog().cancel();
			}
		});

		return rootView;

	}//End public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)


	private void showMessage(final String toastMsg) {
		Toast.makeText(getActivity(), toastMsg, Toast.LENGTH_SHORT).show();
	}


	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if(context instanceof IFromToFragment) {
			iFromToFragment = (IFromToFragment) context;
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		iFromToFragment = null;
	}

}