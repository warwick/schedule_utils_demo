/*
Though package 'com.WarwickWestonWright.ScheduleUtils' is protected by open source, the source code in this package (com.WarwickWestonWright.ScheduleUtilsDemo) is free.
Should you wish to commission the developer you can contact him at: warwickwestonwright@gmail.com
*/
package com.WarwickWestonWright.ScheduleUtilsDemo;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

import com.WarwickWestonWright.ScheduleUtils.AvailabilityCalculator;
import com.WarwickWestonWright.ScheduleUtils.ScheduleItems.ItemActionsValidator;
import com.WarwickWestonWright.ScheduleUtils.ScheduleItems.ScheduleItem;
import com.WarwickWestonWright.ScheduleUtils.UtilityFunctions.TimeConverter;
import com.WarwickWestonWright.ScheduleUtils.UtilityFunctions.UtilityFunctions;
import com.WarwickWestonWright.ScheduleUtils.WorkingHours.FromToByDay;
import com.WarwickWestonWright.ScheduleUtilsDemo.Dialogs.AlertDlgFrag;
import com.WarwickWestonWright.ScheduleUtilsDemo.Dialogs.DatePickerFragment;
import com.WarwickWestonWright.ScheduleUtilsDemo.Dialogs.DurationFragment;
import com.WarwickWestonWright.ScheduleUtilsDemo.Dialogs.FromToFragment;
import com.WarwickWestonWright.ScheduleUtilsDemo.Dialogs.SlotsFragment;
import com.WarwickWestonWright.ScheduleUtilsDemo.Dialogs.TimePickerFragment;
import com.WarwickWestonWright.ScheduleUtilsDemo.room.Appointment;
import com.WarwickWestonWright.ScheduleUtilsDemo.room.ScheduleHoursDB;
import com.WarwickWestonWright.ScheduleUtilsDemo.room.WorkingHours;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import static com.WarwickWestonWright.ScheduleUtils.LibConstants.MILLISECONDS_PER_DAY;

public class ItemsFragment extends Fragment implements
	DatePickerDialog.OnDateSetListener,
	TimePickerDialog.OnTimeSetListener,
	DurationFragment.IDurationFragment,
	FromToFragment.IFromToFragment,
	AppointmentListFragment.IAppointmentListFragment,
	SlotsFragment.ISlotsFragment,
	AlertDlgFrag.IAlertDlgFrag,
	Runnable {

	public interface IItemsFragment {void itemsFragmentCB(List<ScheduleItem> scheduleItems);}

	private final ItemsFragmentHandler itemsFragmentHandler = new ItemsFragmentHandler(this);

	private IItemsFragment iItemsFragment;
	private ScheduleHoursDB scheduleHoursDB;
	private DatePickerFragment datePickerFragment;
	private TimePickerFragment timePickerFragment;
	private DurationFragment durationFragment;
	private FromToFragment fromToFragment;
	private SlotsFragment slotsFragment;
	private AlertDlgFrag alertDlgFrag;
	private boolean alertDlgFragShowing = false;

	private AppointmentListFragment appointmentListFragment;

	private List<Appointment> appointments = new ArrayList<>();
	private List<ScheduleItem> scheduleItems = new ArrayList<>();
	private List<WorkingHours> workingHours = new ArrayList<>();
	private List<ScheduleItem> availSlots;
	private final List<FromToByDay> fromToByDays = new ArrayList<>();
	private ScheduleItem scheduleItem;

	private View rootView;
	private Button btnAddItem;
	private Button btnGetSlots;
	private GregorianCalendar calendar;
	private SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private SharedPreferences sp;
	private String utcStartTime;
	private String friendlyDuration;
	private String utcEndTime;
	private Date startTime;
	private Date endTime;

	private Thread fetchItemsThread;

	private ItemActionsValidator itemActionsValidator;
	private final TimeConverter timeConverter = new TimeConverter();

	private int year;
	private int month;
	private int day;
	private int hour;
	private int minute;
	private long duration;
	private long appointmentId;
	private int itemSelected;

	private static final int NO_ACTION = 0;
	private static final int DELETE_APPOINTMENT = 1;
	private static final int  ADD_APPOINTMENT = 2;
	private static final int GET_ALL_APPOINTMENTS = 3;
	private static final int EDIT_APPOINTMENT = 4;
	private static final int GET_ALL_HOURS = 5;
	private int formAction;
	private AvailabilityCalculator avCalc;
	private final UtilityFunctions utilityFunctions = new UtilityFunctions();

	private final ItemsFragment context = this;

	public ItemsFragment() {}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		scheduleHoursDB = ScheduleHoursDB.getDatabase(getActivity().getApplicationContext());
		formAction = GET_ALL_APPOINTMENTS;
		itemSelected = -1;
		sp = getActivity().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
		getGetAllAppointments();
	}//End public void onCreate(Bundle savedInstanceState)


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.items_fragment, container, false);
		btnAddItem = rootView.findViewById(R.id.btnAddItem);
		btnGetSlots = rootView.findViewById(R.id.btnGetSlots);

		btnAddItem.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(datePickerFragment == null) {datePickerFragment = new DatePickerFragment();}
				formAction = ADD_APPOINTMENT;
				itemSelected = -1;
				datePickerFragment.setTargetFragment(getFragmentManager().findFragmentByTag("ItemsFragment"), 0);
				datePickerFragment.show(getFragmentManager(), "DatePickerFragment");
			}
		});

		btnGetSlots.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				final Bundle args = new Bundle();
				final Date fromTime = new Date();
				final Date toTime = new Date(fromTime.getTime() + MILLISECONDS_PER_DAY);

				final ScheduleItem scheduleItem = new ScheduleItem(timeConverter.getDateFromMySqlFormat(sp.getString("from", timeConverter.getMysqlFormatFromDate(fromTime))),
					timeConverter.getDateFromMySqlFormat(sp.getString("to", timeConverter.getMysqlFormatFromDate(toTime))));

				args.putParcelable("fromTo", scheduleItem);
				fromToFragment = new FromToFragment();
				fromToFragment.setArguments(args);
				fromToFragment.setTargetFragment(getFragmentManager().findFragmentByTag("ItemsFragment"), 0);
				fromToFragment.setCancelable(false);
				fromToFragment.show(getFragmentManager(), "FromToFragment");

			}
		});

		return rootView;

	}//End public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)


	private void getGetAllAppointments() {
		if(fetchItemsThread == null) {fetchItemsThread = new Thread(context);}
		fetchItemsThread.start();
	}


	@Override
	public void alertDlgFragCb(String buttonsLabel) {

		if(alertDlgFrag != null && alertDlgFrag.isAdded() == true) {
			alertDlgFrag.getDialog().cancel();
			alertDlgFrag.onDetach();
			alertDlgFragShowing = false;
		}

		if(buttonsLabel != null && buttonsLabel.equals("Delete")) {
			formAction = DELETE_APPOINTMENT;
			if(fetchItemsThread == null) {fetchItemsThread = new Thread(context);}
			fetchItemsThread.start();
		}
		else if(buttonsLabel != null && buttonsLabel.equals("Edit")) {
			if(datePickerFragment == null) {datePickerFragment = new DatePickerFragment();}
			formAction = EDIT_APPOINTMENT;
			datePickerFragment.setTargetFragment(getFragmentManager().findFragmentByTag("ItemsFragment"), 0);
			datePickerFragment.show(getFragmentManager(), "DatePickerFragment");
		}
		else if(buttonsLabel != null && buttonsLabel.equals("Cancel")) {
			formAction = NO_ACTION;
			showMessage(buttonsLabel);
		}//End if(buttonsLabel != null && buttonsLabel.equals("Delete"))

	}//End public void alertDlgFragCb(String buttonsLabel)


	@Override
	public void onDateSet(DatePicker view, int year, int month, int day) {
		this.year = year; this.month = month; this.day = day;
		calendar = new GregorianCalendar(year, month, day);
		setDate();
	}


	private void setDate() {
		sdf = new SimpleDateFormat("yyyy-MM-dd");
		if(timePickerFragment == null) {timePickerFragment = new TimePickerFragment();}
		timePickerFragment.setTargetFragment(getFragmentManager().findFragmentByTag("ItemsFragment"), 0);
		timePickerFragment.show(getFragmentManager(), "TimePickerFragment");
	}


	@Override
	public void onTimeSet(TimePicker view, int hour, int minute) {
		this.hour = hour; this.minute = minute;
		calendar = new GregorianCalendar(year, month, day, hour, minute);
		sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		utcStartTime = sdf.format(calendar.getTime());
		startTime = calendar.getTime();
		durationFragment = new DurationFragment();
		final Bundle args = new Bundle();

		if(formAction == ADD_APPOINTMENT) {
			args.putString("action", "Add Item");
		}
		else if(formAction == EDIT_APPOINTMENT) {
			final String friendlyDuration = timeConverter.getFriendlyDuration(appointments.get(itemSelected).getDuration());
			args.putString("action", "Edit Item");
			args.putString("hmStr", friendlyDuration);
		}

		durationFragment.setArguments(args);
		durationFragment.setTargetFragment(getFragmentManager().findFragmentByTag("ItemsFragment"), 0);
		durationFragment.setCancelable(false);
		durationFragment.show(getFragmentManager(), "DurationFragment");
	}


	@Override
	public void durationFragmentCB(long duration) {

		this.duration = duration;
		if(durationFragment != null && durationFragment.isAdded() == true) {durationFragment.dismiss();}
		endTime = new Date(calendar.getTimeInMillis() + duration);
		utcEndTime = sdf.format(endTime);
		friendlyDuration = timeConverter.getFriendlyDuration(duration);
		scheduleItem = new ScheduleItem(startTime, duration);
		itemActionsValidator = new ItemActionsValidator(scheduleItems);

		final int validationResult = itemActionsValidator.validate(scheduleItem, itemSelected);//-1 for adding records or index of item being edited.

		if(validationResult == -1) {
			if(fetchItemsThread == null) {fetchItemsThread = new Thread(context);}
			fetchItemsThread.start();//Executes addItem() or editItem() depending on value of formAction
		}
		else if(validationResult > -1) {
			showMessage("Conflicts with idx: " + Integer.toString(validationResult));
		}

	}//End public void durationFragmentCB(long duration)


	@Override
	public void fromToFragmentCB(final ScheduleItem scheduleItem) {
		if(fromToFragment != null && fromToFragment.isAdded()) {fromToFragment.getDialog().cancel();}
		sp.edit().putString("from", timeConverter.getMysqlFormatFromDate(scheduleItem.getStartTime())).apply();
		sp.edit().putString("to", timeConverter.getMysqlFormatFromDate(scheduleItem.getEndTime())).apply();
		avCalc = new AvailabilityCalculator(scheduleItems, fromToByDays);
		final Bundle args = new Bundle();
		availSlots = avCalc.getAvailableTimes(scheduleItem.getStartTime(), scheduleItem.getEndTime(), scheduleItems, fromToByDays);
		args.putParcelableArrayList("availSlots", (ArrayList<? extends Parcelable>) availSlots);
		slotsFragment = new SlotsFragment();
		slotsFragment.setArguments(args);
		slotsFragment.setCancelable(false);
		slotsFragment.setTargetFragment(getActivity().getSupportFragmentManager().findFragmentByTag("ItemsFragment"), 0);
		slotsFragment.show(getActivity().getSupportFragmentManager(), "SlotsFragment");
	}//End public void fromToFragmentCB(final ScheduleItem scheduleItem)


	@Override
	public void slotsFragmentCB() {
		if(slotsFragment != null && slotsFragment.isAdded()) {slotsFragment.getDialog().cancel();}
	}


	private void getAllAppointments() {
		appointments = scheduleHoursDB.appointmentsDAO().getAllAppointments();
		scheduleItems.clear();
		final int aptSize = appointments.size();
		for(int i = 0; i < aptSize; i++) {
			scheduleItems.add(new ScheduleItem(timeConverter.getDateFromMySqlFormat(appointments.get(i).getStart()), appointments.get(i).getDuration()));
		}
	}//End private void getAllAppointments()


	private void deleteItem(final long appointmentId) {
		scheduleHoursDB.appointmentsDAO().deleteAppointment(appointmentId);
	}


	private void addItem() {
		final Appointment appointment = new Appointment(calendar.getTimeInMillis(), utcStartTime, duration, utcEndTime);
		appointments.add(appointment);
		scheduleItems.add(scheduleItem);
		scheduleHoursDB.appointmentsDAO().insertItems(appointment);
	}


	private void editItem(int itemSelected) {

		final long editId = scheduleItems.get(itemSelected).getStartTime().getTime();
		scheduleHoursDB.appointmentsDAO().deleteAppointment(editId);
		final Appointment appointment = new Appointment(calendar.getTimeInMillis(), utcStartTime, duration, utcEndTime);
		appointments.add(appointment);
		scheduleItems.add(scheduleItem);
		scheduleHoursDB.appointmentsDAO().insertItems(appointment);

	}


	private void getGetAllHours() {
		workingHours = scheduleHoursDB.hoursDao().getAllHours();
		fromToByDays.clear();
		//Convert app workingHours object to library FromToByDay object
		for(WorkingHours hour : workingHours) {
			fromToByDays.add(new FromToByDay(hour.getFrom(), hour.getTo(), hour.getDay()));
		}
	}


	@Override
	public void appointmentListFragmentCB(final Appointment appointment, final int itemSelected) {

		appointmentId = appointment.getId();
		this.itemSelected = itemSelected;
		if(alertDlgFrag == null) {alertDlgFrag = new AlertDlgFrag();}
		if(alertDlgFrag.isAdded() == false && alertDlgFragShowing == false) {

			Bundle args = new Bundle();
			args.putString("title", "What Would You Like To Do?");
			String msg = "Start: " + appointment.getStart() + "\nDuration: " + timeConverter.getFriendlyDuration(appointment.getDuration()) + "\nEnd: " + appointment.getEnd();
			args.putString("msg", msg);
			args.putStringArray("buttons", new String[] {"Delete", "Edit", "Cancel"});
			alertDlgFrag.setArguments(args);
			alertDlgFrag.setTargetFragment(getFragmentManager().findFragmentByTag("ItemsFragment"), 0);
			alertDlgFragShowing = true;
			alertDlgFrag.setCancelable(false);
			alertDlgFrag.show(getFragmentManager(), "AlertDlgFrag");

		}//End if(alertDlgFrag.getShowsDialog() == false)
	}


	private void showMessage(final String toastMsg) {
		Toast.makeText(getActivity(), toastMsg, Toast.LENGTH_SHORT).show();
	}


	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if(context instanceof IItemsFragment) {
			iItemsFragment = (IItemsFragment) context;
		}
		else {
			throw new RuntimeException(context.toString() + " must implement IItemsFragment");
		}
	}


	@Override
	public void onDetach() {
		super.onDetach();
		iItemsFragment = null;
	}


	private void attachListFragment() {
		Bundle args = new Bundle();
		args.putParcelableArrayList("appointments", (ArrayList<? extends Parcelable>) appointments);
		appointmentListFragment = new AppointmentListFragment();
		appointmentListFragment.setArguments(args);
		getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.appointmentsListContainer, appointmentListFragment, "AppointmentListFragment").commit();
		formAction = GET_ALL_HOURS;
		if(fetchItemsThread == null) {fetchItemsThread = new Thread(context);}
		fetchItemsThread.start();//Executes getGetAllHours()
	}


	@Override
	public void run() {

		if(formAction == DELETE_APPOINTMENT) {
			deleteItem(appointmentId);
		}
		else if(formAction == ADD_APPOINTMENT) {
			addItem();
		}
		else if(formAction == GET_ALL_APPOINTMENTS) {
			getAllAppointments();
		}
		else if(formAction == EDIT_APPOINTMENT) {
			editItem(itemSelected);
		}
		else if(formAction == GET_ALL_HOURS) {
			getGetAllHours();
		}

		itemsFragmentHandler.sendEmptyMessage(0);
	}


	private static class ItemsFragmentHandler extends Handler {
		private final WeakReference<ItemsFragment> itemsFragmentWeakReference;
		public ItemsFragmentHandler(ItemsFragment itemsFragment) {
			itemsFragmentWeakReference = new WeakReference<>(itemsFragment);
		}

		@Override
		public void handleMessage(Message msg) {

			ItemsFragment itemsFragment = itemsFragmentWeakReference.get();

			if(itemsFragment != null) {
				itemsFragment.fetchItemsThread = null;

				if(itemsFragment.formAction == DELETE_APPOINTMENT) {
					itemsFragment.showMessage("Item with id: " + Long.toString(itemsFragment.appointmentId) + " Deleted");
					itemsFragment.formAction = GET_ALL_APPOINTMENTS;
					itemsFragment.getGetAllAppointments();
				}
				else if(itemsFragment.formAction == ADD_APPOINTMENT) {
					itemsFragment.showMessage("Appointment Added");
					itemsFragment.formAction = GET_ALL_APPOINTMENTS;
					itemsFragment.getGetAllAppointments();
				}
				else if(itemsFragment.formAction == GET_ALL_APPOINTMENTS) {
					itemsFragment.attachListFragment();
				}
				else if(itemsFragment.formAction == EDIT_APPOINTMENT) {
					itemsFragment.showMessage("Appointment Edited");
					itemsFragment.formAction = GET_ALL_APPOINTMENTS;
					itemsFragment.getGetAllAppointments();
				}
				else if(itemsFragment.formAction == GET_ALL_HOURS) {

				}//End if(itemsFragment.formAction == formAction = DELETE_APPOINTMENT)

			}//End if(itemsFragment != null)

		}
	}
}