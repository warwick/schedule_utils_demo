/*
Though package 'com.WarwickWestonWright.ScheduleUtils' is protected by open source, the source code in this package (com.WarwickWestonWright.ScheduleUtilsDemo) is free.
Should you wish to commission the developer you can contact him at: warwickwestonwright@gmail.com
*/
package com.WarwickWestonWright.ScheduleUtilsDemo.Dialogs;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.inputmethodservice.Keyboard;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.WarwickWestonWright.ScheduleUtilsDemo.CustomKeyboardView.FloatingKeyboardView;
import com.WarwickWestonWright.ScheduleUtilsDemo.R;

@TargetApi(21)
public class DurationFragment extends DialogFragment {

	public interface IDurationFragment {void durationFragmentCB(final long duration);}
	private IDurationFragment iDurationFragment;

	private EditText txtHH;
	private EditText txtMM;
	private Button btnAddItem;
	private Button btnCancel;
	private Bundle args;
	private String btnText;
	private String hmStr;
	private FloatingKeyboardView floatingKeyboardView;

	private long duration;
	private View rootView;

	public DurationFragment() {}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if(iDurationFragment == null) {
			iDurationFragment = (IDurationFragment) getTargetFragment();
		}
		if((args = getArguments()) != null) {
			btnText = args.getString("action");
			hmStr = args.getString("hmStr", "01:00");
		}
		else {
			btnText = "Commit";
			hmStr = "01:00";
		}
	}//End public void onCreate(Bundle savedInstanceState)


	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		final Dialog dialog = new Dialog(getActivity());
		dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
			@Override
			public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
				if(event.getAction() == MotionEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
					if(floatingKeyboardView != null && floatingKeyboardView.isShown()) {
						floatingKeyboardView.hide();
						return true;
					}
					return true;
				}
				else {
					return false;
				}
			}
		});
		return dialog;
	}//End public Dialog onCreateDialog(Bundle savedInstanceState)


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.duration_fragment, container, false);
		floatingKeyboardView = rootView.findViewById(R.id.keyboardView);
		floatingKeyboardView.setKeyboard(new Keyboard(getActivity(), R.xml.keyboard_date_time));
		floatingKeyboardView.setPreviewEnabled(false);//NOTE Do not show the preview balloons
		btnAddItem = rootView.findViewById(R.id.btnAddItem);
		btnCancel = rootView.findViewById(R.id.btnCancel);
		final String[] hmStrParts = hmStr.split(":");
		txtHH = rootView.findViewById(R.id.txtHH);
		txtMM = rootView.findViewById(R.id.txtMM);
		//@TargetApi(21) at the top of this class if for the following 2 lines
		txtHH.setShowSoftInputOnFocus(false);//Dev note is supported from api 16 ignore this error
		txtMM.setShowSoftInputOnFocus(false);//Dev note is supported from api 16 ignore this error
		floatingKeyboardView.registerEditText(txtHH);
		floatingKeyboardView.registerEditText(txtMM);

		try {
			Integer.parseInt(hmStrParts[0]);
			Integer.parseInt(hmStrParts[1]);
			txtHH.setText(hmStrParts[0]);
			txtMM.setText(hmStrParts[1]);
		}
		catch(NumberFormatException e) {
			txtHH.setText("01");
			txtMM.setText("00");
		}

		btnAddItem.setText(btnText);

		btnAddItem.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				String hh = txtHH.getText().toString().trim();
				String mm = txtMM.getText().toString().trim();

				if(hh.isEmpty() == true && mm.isEmpty() == true) {
					Toast.makeText(getActivity(), getString(R.string.toast_invalid_duration), Toast.LENGTH_LONG).show();
					return;
				}

				if(hh.isEmpty() == true) {
					hh = "0";
				}

				if(mm.isEmpty() == true) {
					mm = "0";
				}

				if(hh.equals("0") && mm.equals("0")) {
					Toast.makeText(getActivity(), getString(R.string.toast_duration_zero_error), Toast.LENGTH_LONG).show();
					return;
				}

				txtHH = rootView.findViewById(R.id.txtHH);
				txtMM = rootView.findViewById(R.id.txtMM);
				duration = (Long.parseLong(hh) * 3600000L) + (Long.parseLong(mm) * 60000L);
				iDurationFragment.durationFragmentCB(duration);

			}
		});

		btnCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				getDialog().cancel();
			}
		});

		return rootView;

	}//End public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if(context instanceof IDurationFragment) {
			iDurationFragment = (IDurationFragment) context;
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		iDurationFragment = null;
	}
}