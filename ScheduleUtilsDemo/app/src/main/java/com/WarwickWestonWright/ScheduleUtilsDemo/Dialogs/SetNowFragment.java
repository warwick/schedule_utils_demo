package com.WarwickWestonWright.ScheduleUtilsDemo.Dialogs;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.inputmethodservice.Keyboard;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.WarwickWestonWright.ScheduleUtils.UtilityFunctions.TimeConverter;
import com.WarwickWestonWright.ScheduleUtils.WorkingHours.Validators;
import com.WarwickWestonWright.ScheduleUtilsDemo.CustomKeyboardView.FloatingKeyboardView;
import com.WarwickWestonWright.ScheduleUtilsDemo.R;

import java.util.Date;

@TargetApi(21)
public class SetNowFragment extends DialogFragment {

	private View rootView;
	private EditText txtTestTime;
	private Button btnSetTime;
	private Button btnCancelSetTestTime;
	private String testTimeStr;
	private TimeConverter timeConverter;
	private Validators validators;
	private FloatingKeyboardView floatingKeyboardView;
	private SharedPreferences sp;

	public interface ISetNowFragment {void setNowFragmentCB(final Date testTime);}

	private ISetNowFragment iSetNowFragment;

	public SetNowFragment() {}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		sp = getActivity().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
		timeConverter = new TimeConverter();
		validators = new Validators();
		if(iSetNowFragment == null) {
			iSetNowFragment = (ISetNowFragment) getTargetFragment();
		}
		if(sp.getString("setNow", "").equals("")) {
			testTimeStr = timeConverter.getMysqlFormatFromDate(new Date());
		}
		else {
			testTimeStr = sp.getString("setNow", timeConverter.getMysqlFormatFromDate(new Date()));
		}
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		final Dialog dialog = new Dialog(getActivity());
		dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
			@Override
			public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
				if(event.getAction() == MotionEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
					if(floatingKeyboardView != null && floatingKeyboardView.isShown()) {
						floatingKeyboardView.hide();
						return true;
					}
					return true;
				}
				else {
					return false;
				}
			}
		});
		return dialog;
	}//End public Dialog onCreateDialog(Bundle savedInstanceState)

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_set_now, container, false);
		floatingKeyboardView = rootView.findViewById(R.id.keyboardView);
		floatingKeyboardView.setKeyboard(new Keyboard(getActivity(), R.xml.keyboard_date_time));
		floatingKeyboardView.setPreviewEnabled(false);//NOTE Do not show the preview balloons
		txtTestTime = rootView.findViewById(R.id.txtTestTime);
		btnSetTime = rootView.findViewById(R.id.btnSetTime);
		btnCancelSetTestTime = rootView.findViewById(R.id.btnCancelSetTestTime);
		txtTestTime.setText(testTimeStr);
		//@TargetApi(21) at the top of this class if for the following 2 lines
		txtTestTime.setShowSoftInputOnFocus(false);//Dev note is supported from api 16 ignore this error
		floatingKeyboardView.registerEditText(txtTestTime);//Dev note is supported from api 16 ignore this error

		btnSetTime.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(txtTestTime.getText().toString().equals("") == true) {
					if(iSetNowFragment != null) {
						iSetNowFragment.setNowFragmentCB(null);
					}
				}
				else if(txtTestTime.getText().toString().equals("") == false) {
					testTimeStr = txtTestTime.getText().toString().trim();
					if(testTimeStr.substring(testTimeStr.length() -1).equals(":")) {
						testTimeStr = testTimeStr.substring(0, testTimeStr.lastIndexOf(":"));
					}

					final String[] fromTimeParts = testTimeStr.split(":");

					switch(fromTimeParts.length) {
						case 2:
							testTimeStr += ":00";
							break;
						case 1:
							testTimeStr += ":00:00";
							break;
						case 0:
							testTimeStr += ":00:00:00";
							break;
						default:
							break;
					}

					if(validators.validateMySQLFormat(testTimeStr) == false) {
						showMessage(getString(R.string.msg_invalid_mysql_format));
						return;
					}

					if(iSetNowFragment != null) {
						iSetNowFragment.setNowFragmentCB(timeConverter.getDateFromMySqlFormat(testTimeStr));
					}
					else {
						showMessage(getString(R.string.msg_problem_setting_time));
						getDialog().dismiss();
					}
				}
			}
		});

		btnCancelSetTestTime.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				getDialog().dismiss();
			}
		});

		return rootView;
	}


	private void showMessage(final String toastMsg) {
		Toast.makeText(getActivity(), toastMsg, Toast.LENGTH_SHORT).show();
	}


	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if(context instanceof ISetNowFragment) {
			iSetNowFragment = (ISetNowFragment) context;
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		iSetNowFragment = null;
	}
}