/*
Though package 'com.WarwickWestonWright.ScheduleUtils' is protected by open source, the source code in this package (com.WarwickWestonWright.ScheduleUtilsDemo) is free.
Should you wish to commission the developer you can contact him at: warwickwestonwright@gmail.com
*/
package com.WarwickWestonWright.ScheduleUtilsDemo;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.WarwickWestonWright.ScheduleUtilsDemo.Adapters.AppointmentAdapter;
import com.WarwickWestonWright.ScheduleUtilsDemo.room.Appointment;

import java.util.List;

public class AppointmentListFragment extends Fragment {

	public interface IAppointmentListFragment {void appointmentListFragmentCB(final Appointment appointment, final int itemSelected);}

	private View rootView;
	private static final String ARG_COLUMN_COUNT = "column-count";
	private final int columnCount = 1;
	private IAppointmentListFragment iAppointmentListFragment;
	private List<Appointment> appointments;

	public AppointmentListFragment() {}

	@SuppressWarnings("unused")
	public static AppointmentListFragment newInstance(int columnCount) {
		AppointmentListFragment fragment = new AppointmentListFragment();
		Bundle args = new Bundle();
		args.putInt(ARG_COLUMN_COUNT, columnCount);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if(getArguments() != null) {
			//columnCount = getArguments().getInt(ARG_COLUMN_COUNT);
			appointments = getArguments().getParcelableArrayList("appointments");
		}

		if(iAppointmentListFragment == null) {
			iAppointmentListFragment = (IAppointmentListFragment) getFragmentManager().findFragmentByTag("ItemsFragment");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.appointment_item_list_fragment, container, false);
		setAdapter();
		return rootView;
	}

	public void setAdapter() {
		//Set the adapter
		if(rootView instanceof RecyclerView) {
			final Context context = rootView.getContext();
			RecyclerView recyclerView = (RecyclerView) rootView;
			if(columnCount <= 1) {
				recyclerView.setLayoutManager(new LinearLayoutManager(context));
			}
			else {
				recyclerView.setLayoutManager(new GridLayoutManager(context, columnCount));
			}
			recyclerView.setAdapter(new AppointmentAdapter(appointments, iAppointmentListFragment, recyclerView));

		}
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if(context instanceof IAppointmentListFragment) {
			iAppointmentListFragment = (IAppointmentListFragment) context;
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		iAppointmentListFragment = null;
	}

}