/*
Though package 'com.WarwickWestonWright.ScheduleUtils' is protected by open source, the source code in this package (com.WarwickWestonWright.ScheduleUtilsDemo) is free.
Should you wish to commission the developer you can contact him at: warwickwestonwright@gmail.com
*/
package com.WarwickWestonWright.ScheduleUtilsDemo;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnItemTouchListener;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

public class RecyclerTouchListener implements OnItemTouchListener {

	private final GestureDetector gestureDetector;
	private final ClickListener clickListener;

	public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
		this.clickListener = clickListener;
		gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
			@Override
			public boolean onSingleTapUp(MotionEvent e) {
				return true;
			}

			@Override
			public void onLongPress(MotionEvent e) {
				View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
				if(child != null && clickListener != null) {
					clickListener.onLongClick(child, recyclerView.getLayoutManager().getPosition(child));
				}
			}
		});
	}

	@Override
	public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
		View child = rv.findChildViewUnder(e.getX(), e.getY());
		if(child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {

			if(e.getAction() == MotionEvent.ACTION_UP) {
				clickListener.onClick(child, rv.getLayoutManager().getPosition(child));
				return true;
			}
		}
		return false;
	}
	@Override
	public void onTouchEvent(RecyclerView rv, MotionEvent e) {}
	@Override
	public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {}

	public interface ClickListener {
		void onClick(View view, int position);
		void onLongClick(View view, int position);
	}

}