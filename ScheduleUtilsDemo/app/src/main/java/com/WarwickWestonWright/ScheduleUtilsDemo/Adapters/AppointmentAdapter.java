/*
Though package 'com.WarwickWestonWright.ScheduleUtils' is protected by open source, the source code in this package (com.WarwickWestonWright.ScheduleUtilsDemo) is free.
Should you wish to commission the developer you can contact him at: warwickwestonwright@gmail.com
*/
package com.WarwickWestonWright.ScheduleUtilsDemo.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.WarwickWestonWright.ScheduleUtils.UtilityFunctions.TimeConverter;
import com.WarwickWestonWright.ScheduleUtilsDemo.AppointmentListFragment.IAppointmentListFragment;
import com.WarwickWestonWright.ScheduleUtilsDemo.R;
import com.WarwickWestonWright.ScheduleUtilsDemo.RecyclerTouchListener;
import com.WarwickWestonWright.ScheduleUtilsDemo.room.Appointment;

import java.util.List;

public class AppointmentAdapter extends RecyclerView.Adapter<AppointmentAdapter.ViewHolder> {

	private final List<Appointment> appointments;
	private final IAppointmentListFragment iAppointmentListFragment;
	private final TimeConverter timeConverter = new TimeConverter();
	private final RecyclerView recyclerView;

	public AppointmentAdapter(final List<Appointment> appointments, final IAppointmentListFragment iAppointmentListFragment, final RecyclerView recyclerView) {
		this.appointments = appointments;
		this.iAppointmentListFragment = iAppointmentListFragment;
		this.recyclerView = recyclerView;
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.appointment_item_layout, parent, false);
		return new ViewHolder(view);
	}

	@Override
	public void onBindViewHolder(final ViewHolder holder, final int position) {
		holder.appointment = appointments.get(position);
		holder.lblId.setText("Id as start time in milliseconds (from epoc):\n" + Long.toString(appointments.get(position).getId()));
		holder.lblStart.setText("Start Time: " + appointments.get(position).getStart());
		holder.lbDuration.setText("Duration: " + timeConverter.getFriendlyDuration(appointments.get(position).getDuration()));
		holder.lbEnd.setText("End Time: " + appointments.get(position).getEnd());
		holder.view.setTag(Integer.toString(position));

		recyclerView.addOnItemTouchListener(new RecyclerTouchListener(recyclerView.getContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
			@Override
			public void onClick(View view, int position) {
				iAppointmentListFragment.appointmentListFragmentCB(appointments.get(position), position);
			}
			@Override
			public void onLongClick(View view, int position) {}
		}));

	}


	@Override
	public int getItemCount() {
		return appointments.size();
	}

	public class ViewHolder extends RecyclerView.ViewHolder {

		public final View view;
		public final TextView lblId;
		public final TextView lblStart;
		public final TextView lbDuration;
		public final TextView lbEnd;
		public Appointment appointment;

		public ViewHolder(View view) {
			super(view);
			this.view = view;
			lblId = view.findViewById(R.id.lblId);
			lblStart = view.findViewById(R.id.lblStart);
			lbDuration = view.findViewById(R.id.lbDuration);
			lbEnd = view.findViewById(R.id.lbEnd);
		}

		@Override
		public String toString() {return super.toString() + " '" + lblStart.getText() + "'";}

	}

}